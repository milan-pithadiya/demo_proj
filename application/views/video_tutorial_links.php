<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
      <link rel="stylesheet" href="<?=base_url('assets/css/jquery.dataTables.min.css')?>">
    </head>
    <body>
        <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          <!-- about start -->
            <div class="allpage_banner_email allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'email.jpg')?>);">
              <h1 class="title_h1">TUTORIALS</h1>
              <p><a href="<?=base_url()?>">Home </a> / TUTORIALS</p>
            </div>
            <!-- Tutorial Strat Here -->
            <div class="tutorial">
              <div class="container padding_all">
                <div class="row">
                  <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <div class="tutorial-link">
                      <div class="title" style="margin-bottom: 20px;">
                        <h2><b><u>Plesk Onyx 2017 End User Tutorials</u></b></h2>
                      </div>
                      <li><a href="<?=base_url('tutorials/view_video_tutorial/1')?>"><i class="fa fa-file"></i>  How to find and install applications in Plesk</a></li>
                      <li><a href="<?=base_url('tutorials/view_video_tutorial/1')?>"><i class="fa fa-file"></i>  How to manage DNS zones in Plesk</a></li>
                      <li><a href="<?=base_url('tutorials/view_video_tutorial/1')?>"><i class="fa fa-file"></i>  How to suspend websites in Plesk</a></li>
                      <li><a href="<?=base_url('tutorials/view_video_tutorial/1')?>"><i class="fa fa-file"></i>  How to install SSL certificates in Plesk</a></li>
                      <li><a href="<?=base_url('tutorials/view_video_tutorial/1')?>"><i class="fa fa-file"></i>  How to password protect a directory in Plesk</a></li>
                      <li><a href="<?=base_url('tutorials/view_video_tutorial/1')?>"><i class="fa fa-file"></i>  How to setup scheduled tasks in Plesk</a></li>
                      <li><a href="<?=base_url('tutorials/view_video_tutorial/1')?>"><i class="fa fa-file"></i>  How to perform website backups in Plesk</a></li>
                      <li><a href="<?=base_url('tutorials/view_video_tutorial/1')?>"><i class="fa fa-file"></i>  How to setup web users in Plesk</a></li>
                      <li><a href="<?=base_url('tutorials/view_video_tutorial/1')?>"><i class="fa fa-file"></i>  How to setup a database in Plesk</a></li>
                      <li><a href="<?=base_url('tutorials/view_video_tutorial/1')?>"><i class="fa fa-file"></i>  How to setup a WordPress site in Plesk</a></li>
                      <li><a href="<?=base_url('tutorials/view_video_tutorial/1')?>"><i class="fa fa-file"></i>  How to login to Plesk</a></li>
                      <li><a href="<?=base_url('tutorials/view_video_tutorial/1')?>"><i class="fa fa-file"></i>  How to create an email account in Plesk</a></li>
                      <li><a href="<?=base_url('tutorials/view_video_tutorial/1')?>"><i class="fa fa-file"></i>  How to create an email alias in Plesk</a></li>
                      <li><a href="<?=base_url('tutorials/view_video_tutorial/1')?>"><i class="fa fa-file"></i>  How to create a catchall email account in Plesk</a></li>
                      <li><a href="<?=base_url('tutorials/view_video_tutorial/1')?>"><i class="fa fa-file"></i>   How to create an email forwarder in Plesk</a></li>
                      <a href="video_tutorial.php" class="btn btn-primary">Back</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Tutorial End Here -->
            
            
      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?> 
   </body>
</html> 
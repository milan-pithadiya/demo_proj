<?php include "admin_panel/includes/connection.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<?php include "include/header-js.php"; ?>
		<link rel="stylesheet" type="text/css" href="popup_css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="popup_css/demo.css" />
		<!-- common styles -->
		<link rel="stylesheet" type="text/css" href="popup_css/dialog.css" />
		<!-- individual effect -->
		<link rel="stylesheet" type="text/css" href="popup_css/dialog-sally.css" />
		<script src="popup_js/modernizr.custom.js"></script>
</head>
<body>
	<!-- main start -->
		<div class="main">
			<?php include "include/header.php"; ?>
			<a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
			<!-- header over -->
			<div class="allpage_banner_domain allpage_banner" id="top">
				<h1 class="title_h1">Domain</h1>
				<p><a href="index.php">Home </a> / Domain</p>
			</div>
			<!-- domain start -->
			<div class="all_domain">
				<div class="domain_box">
				<div class="container padding_all">
					<div class="">
						<h3>Find your Perfect Domain Name:</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia aliquam tempora deleniti quibusdam labore quo fuga.</p>
						<div class="col-md-12 col-xs-12 margin_top">
							<div class="search_domain_box">
								<input type="text" name="">
								  <select id="sel1" class="select_op">
								    <option>.com</option>
								    <option>.net</option>
								    <option>.org</option>
								    <option>.in</option>
								    <option>.eu</option>
								    <option>.name</option>
								  </select>
								<button class="btn_order trigger" data-dialog="somedialog">Search Domain</button>
							</div><hr>
							<h4><span>.com$25</span> | <span>.net$25</span> | <span>.org$25</span> | <span>.in$25</span> | <span>.biz$25</span></h4>
						</div>
					</div>
				</div>
				<div id="somedialog" class="dialog">
					<div class="dialog__overlay"></div>
					<div class="dialog__content">
						<form method="#" name="f1_contact">
							<div class="form-group">
								<label class="ord_class_label">Name</label>
								<input type="text" name="name" id="name" class="input_all ord_class_text">
							</div>
							<div class="form-group">
								<label class="ord_class_label">Email</label>
								<input type="email" name="email" id="email" class="input_all ord_class_text">
							</div>
							<div class="form-group">
								<label class="ord_class_label">Mobile Number</label>
								<input type="text" name="number" id="number" class="input_all ord_class_text">
							</div>
							<div class="form-group text-center">
								<button class="btn_order">Submit</button>
							</div>
						</form>
						<hr>
						<div style="float: right;margin-right: 4%">
							<button class="btn_order action" data-dialog-close>Close</button>
						</div>
					</div>
				</div>
				</div>
				<div class="container padding_all">
				<?php $str_do="select * from domain where status=1";
					$ex_do=mysqli_query($db,$str_do);
					if(mysqli_num_rows($ex_do)>0)
					{
					$i=1;
						while($row_do=mysqli_fetch_array($ex_do))
						{
				 ?>
							<div class="col-md-2 col-xs-6 <?php if($i%2==1){?> color_domain_one <?php }else{?> color_domain_two <?php } ?>text-center">
								<h3><?php echo $row_do['name']; ?></h3>
								<p>&nbsp;</p>
								<h2>$ <?php echo $row_do['price']; ?></h2>
								<h6>/year</h6>
							</div>
				<?php $i++;	} } ?>	
				</div>
				<div class="container"><div class="row">
			<?php $str_d="select * from domain_information where status=1";
				$ex_d=mysqli_query($db,$str_d);
				if(mysqli_num_rows($ex_d)>0)
				{
					while($row_d=mysqli_fetch_array($ex_d))
					{
			 ?>
					<div class="col-md-3 col-xs-12 text_h3" style="margin-top:15px;margin-bottom:25px;">
						<i class="fa fa-check-square-o"></i>
						<p><?php echo $row_d['name']; ?></p>
					</div>
			<?php } } ?>		
				</div>
				

			</div>
			<!-- domain over -->
			<!-- footer us start -->
			<?php include "include/footer.php"; ?>
			<!-- footer us over -->
		</div>
		<!-- main over -->
		<script src="popup_js/classie.js"></script>
		<script src="popup_js/dialogFx.js"></script>
		<script>
			(function() {

				var dlgtrigger = document.querySelector( '[data-dialog]' ),
					somedialog = document.getElementById( dlgtrigger.getAttribute( 'data-dialog' ) ),
					dlg = new DialogFx( somedialog );

				dlgtrigger.addEventListener( 'click', dlg.toggle.bind(dlg) );

			})();
		</script>
</body>
</html>
<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('include/header');
?>

<!-- Start main-content -->
<div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-8" data-bg-img="images/bg/bg6.jpg">
        <div class="container pt-60 pb-60">
            <!-- Section Content -->
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2 class="title">FAQ</h2>
                        <ol class="breadcrumb text-center text-black mt-10">
                            <li><a href="<?= base_url()?>">Home</a></li>
                            <!-- <li><a href="#">Pages</a></li> -->
                            <li class="active text-theme-colored">Faq</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="faq-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="accordion1" class="panel-group accordion transparent">
                        <?php
                            if ($faq_details !=null){
                                $counter = 0;
                                foreach ($faq_details as $key => $value) {
                                    $count = $counter.$key+1; 
                                ?>
                                    <div class="panel">
                                        <div class="panel-title"> <a data-parent="#accordion1" data-toggle="collapse" href="#accordion1<?= $key+1?>" class="<?= $count == 1 ? 'active' : '';?>" aria-expanded="<?= $count == 1 ? 'true' : '';?>"> <span class="open-sub"></span> <strong>Q.<?= $key+1?> <?= $value['title']?></strong></a> </div>
                                        <div id="accordion1<?= $key+1?>" class="panel-collapse collapse <?= $count == 1 ? 'in' : '';?>" role="tablist" aria-expanded="<?= $count == 1 ? 'true' : '';?>">
                                            <div class="panel-content">
                                                <p><?= $value['description']?></p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } 
                            } 
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- end main-content -->
<?php $this->load->view('include/copyright');?>
<?php $this->load->view('include/footer');?>

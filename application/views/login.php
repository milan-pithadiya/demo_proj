<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
      <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body>
        <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          <div class="allpage_banner_login allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'login.jpg')?>);">
            <h1 class="title_h1">Login</h1>
            <p><a href="#javascript:;">Home </a> / Login</p>
          </div>
          <div class="all_white padding_all">
            <div class="container">
              <div class="row">
                <div class="login_box col-md-offset-3 col-md-6 col-xs-12">
                  <h3>Login <span class="fa fa-sign-in"></span></h3>
                </div>
              </div>
              <div class="row">
                <form method="post" id="login-form" name="login_form" >
                  <div class="col-md-offset-3 col-md-6 col-xs-12 contact_box_input">

                    <div class="form-group">
                          <label for="email">Email:</label>
                          <input type="text" class="form-control " name="email" id="email" placeholder="Enter email" value="">
                      <p id="err_email"></p>
                      </div>
                      <div class="form-group">
                          <label for="pwd">Password:</label>
                          <input type="password" name="password" class="form-control " id="password" placeholder="Enter password" autocomplete value="<?php if(isset($_COOKIE["member_password"])) { echo $_COOKIE["member_password"]; } ?>">
                      </div>
                     
                      <label>
                          <input type="checkbox" name="remember_me" value="yes" <?php echo (isset($remember_me) && $remember_me == "yes") ? "checked='checked'" : ""; ?>> Remember Me
                      </label>
                      
                      <!-- <div>
                          <div class="g-recaptcha" data-sitekey="6Le-wvkSAAAAAPBMRTvw0Q4Muexq9bi0DJwx_mJ-" style="margin-bottom: 2%;"></div>
                          <span class="error_captcha" style="color: #fc3a3a;"></span>   
                      </div> -->

                      <div class="text-center btn_margin">
                        <button type="submit" name="submit_login" class="btn btn-primary check" style="background-color: #ff802b;border-color: #ff802b;border-radius: 4px">Login <span class="loading-icon"></span></button>
                      <!-- <input type="button" name="submit_login" value="Login" class="btn btn-primary check" style="background-color: #ff802b;border-color: #ff802b;"> -->
                      <a href="<?=base_url('forgot_password')?>" class="btn btn-primary" style="background-color: #ff802b;border-color: #ff802b;"> Forgot Password? </a>
                        
                    </div>
                  </div>
                </form> 
              </div>
              <div class="row">
                <div class="footer_login_box col-md-offset-3 col-md-6 col-xs-12">
                </div>
              </div>
            </div>
          </div>
      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?>

      <script type="text/javascript">
        $('#login-form').validate({
            rules: {
                email: {
                    required: true,
                    email:true,
                },
                password: {
                    required: true,
                },
                
            },
            messages: {
                email: {
                  required: "Please enter Email ",
                  email:"Please enter valid Email",                     
                },
                password: {
                    required: "Please enter password",
                },
            }
        });

        var form = $( "#login-form" );
        form.validate();
        $(document).on('submit','#login-form',function(e){
          //alert( "Valid: " + form.valid() );
          if(form.valid()){
            e.preventDefault();
            get_login();
          }else{
            return false;
          }
     
        });
   
        function get_login(){
           var formData = new FormData($('#login-form')[0]);
            var uurl = BASE_URL+"api/user/login";
            var dashboardURL = BASE_URL+"dashboard";
            $.ajax({
               url: uurl,
               type: 'POST',
               dataType: 'json',
               data: formData,
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('.loading-icon').html('<i class="fa fa-refresh fa-spin"></i>');
               },
               success: function(response){
                if (response.result=="Success") {
                    /*$.alert({
                        type: 'green',
                        title: 'Login success',
                        content: response.message,
                    });*/
                    setTimeout(function() { window.location.href = dashboardURL; }, 2000);
                }else if(response.result=="Fail"){
                    $.alert({
                        type: 'red',
                        title: 'Login failed',
                        content: response.message,
                    });
                    $('.loading-icon').html('<i class="fa fa-refresh"></i>');
                }
                // console.log(response);
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
               cache: false,
               contentType: false,
               processData: false
            });
        }
      </script>
   </body>
</html> 
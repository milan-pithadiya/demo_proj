<!-- SEO Meta -->
<?php // Product and blog wise meta details
    if ($this->uri->segment(2) == 'product_details') {
        $slug_details = $this->Production_model->get_all_with_where('product_manage','','',array('product_id'=>$this->uri->segment(3)));
    }
    elseif ($this->uri->segment(1) == 'blog-detail') {
        $slug_details = $this->Production_model->get_all_with_where('blog','','',array('id'=>$this->uri->segment(2)));
    }

    if (isset($slug_details) && $slug_details !=null) {
    ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="description" content="<?= $slug_details[0]['meta_description']?>">
        <meta name="keywords" content="<?= $slug_details[0]['meta_keywords']?>">
        <meta name="title" content="<?= $slug_details[0]['meta_title']?>">
        <meta name="distribution" content="global">
        <meta name="revisit-after" content="2 Days">
        <meta name="robots" content="ALL">
        <meta name="rating" content="8 YEARS">
        <meta name="Language" content="en-us">
        <meta name="GOOGLEBOT" content="NOARCHIVE">
        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php } 
    else{ // Page wise metatag added.
        $where['seo_menu.menu_name'] = $this->uri->segment(1) == '' ? 'home' : $this->uri->segment(1) ;
            
        $join[0]['table_name'] = 'seo_menu';
        $join[0]['column_name'] = 'seo_menu.menu_id = seo_page_manage.menu_id';
        $join[0]['type'] = 'left';

        $get_metatag = $this->Production_model->jointable_descending(array('seo_menu.menu_name','seo_page_manage.*'),'seo_page_manage','',$join,'id','desc',$where,'','','');
        // echo"<pre>"; echo $this->db->last_query(); exit;
        ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="description" content="<?= isset($get_metatag) && $get_metatag !=null ? $get_metatag[0]['meta_description'] : '';?>">
        <meta name="keywords" content="<?= isset($get_metatag) && $get_metatag !=null ? $get_metatag[0]['meta_keywords'] : '';?>">
        <meta name="title" content="<?= isset($get_metatag) && $get_metatag !=null ? $get_metatag[0]['meta_title'] : '';?>">
        <meta name="distribution" content="global">
        <meta name="revisit-after" content="2 Days">
        <meta name="robots" content="ALL">
        <meta name="rating" content="8 YEARS">
        <meta name="Language" content="en-us">
        <meta name="GOOGLEBOT" content="NOARCHIVE">
        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php }
?>
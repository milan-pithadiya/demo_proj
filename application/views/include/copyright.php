    <!-- Footer Area Start Here -->
<footer>
    <div class="footer-area-top s-space-equal">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="footer-box">
                        <h3 class="title-medium-light title-bar-left size-lg">Company</h3>
                        <ul class="useful-link">
                            
                            <li>
                                <a href="<?= base_url('about-us')?>">About us</a>
                            </li>
                            <li>
                                <a href="<?= base_url('leadership')?>">Leadership</a>
                            </li>
                            <!--<li>
                                <a href="<?= base_url('sitemap')?>">Sitemap</a>
                            </li>
                            <li>
                                <a href="<?= base_url('help')?>">Help</a>
                            </li>-->
                            <li>
                                <a href="<?= base_url('contact')?>">Contact us</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="footer-box">
                        <h3 class="title-medium-light title-bar-left size-lg">Policy</h3>
                        <ul class="useful-link">
                            <li>
                                <a href="<?= base_url('copyright-policy')?>">Copyright policy</a>
                            </li>
                            <li>
                                <a href="<?= base_url('hyperlinking-policy')?>">Hyperlinking policy</a>
                            </li>
                            <li>
                                <a href="<?= base_url('privacy-policy')?>">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="<?= base_url('cookie-policy')?>">Cookie Policy</a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="footer-box">
                        <h3 class="title-medium-light title-bar-left size-lg">Privacy</h3>
                        <ul class="useful-link">
                            
                            
                            <li>
                                <a href="<?= base_url('terms-condition')?>">Terms &amp; conditions</a>
                            </li>
                            <li>
                                <a href="<?= base_url('content-review')?>">Content review policy</a>
                            </li>
                            <li>
                                <a href="<?= base_url('intellectual-property')?>">Intellectual property rights</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="footer-box">
                        <h3 class="title-medium-light title-bar-left size-lg">Follow Us On</h3>
                        <ul class="folow-us">
                            <li>
                                <a href="#">
                                    <img src="<?php echo base_url(); ?>assets/image/footer/follow1.jpg" alt="follow">
                                </a>
                            </li>
                        </ul>
                        <ul class="social-link">
                            <?php
                                $social_media_link = $this->Production_model->get_all_with_where('social_links','','',array());
                            ?>
                            <li>
                                <a href="<?= $social_media_link !=null ? $social_media_link[0]['facebook_link'] : '';?>">
                                    <img src="<?php echo base_url(); ?>assets/image/footer/facebook.png" alt="social">
                                </a>
                            </li>
                            <li>
                                <a href="<?= $social_media_link !=null ? $social_media_link[0]['twiter_link'] : '';?>">
                                    <img src="<?php echo base_url(); ?>assets/image/footer/twitter.png" alt="social">
                                </a>
                            </li>
                            <li>
                                <a href="<?= $social_media_link !=null ? $social_media_link[0]['instagram_link'] : '';?>">
                                    <img src="<?php echo base_url(); ?>assets/image/footer/instagram.png" alt="social">
                                </a>
                            </li>
                            <li>
                                <a href="<?= $social_media_link !=null ? $social_media_link[0]['youtube_link'] : '';?>">
                                    <img src="<?php echo base_url(); ?>assets/image/footer/youtube.png" alt="social">
                                </a>
                            </li>
                            <li>
                                <a href="<?= $social_media_link !=null ? $social_media_link[0]['link_in_link'] : '';?>">
                                    <img src="<?php echo base_url(); ?>assets/image/footer/linkedin.png" alt="social">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-area-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-12 text-center-mb">
                    <p>Copyright © Fireneed</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-12 text-right text-center-mb">
                    <input type="hidden" class="login-user-id" value="<?= $this->session->userdata('login_id')?>">
                    <ul>
                        <li>
                            <img src="<?php echo base_url(); ?>assets/image/footer/card1.jpg" alt="card">
                        </li>
                        <li>
                            <img src="<?php echo base_url(); ?>assets/image/footer/card2.jpg" alt="card">
                        </li>
                        <li>
                            <img src="<?php echo base_url(); ?>assets/image/footer/card3.jpg" alt="card">
                        </li>
                        <li>
                            <img src="<?php echo base_url(); ?>assets/image/footer/card4.jpg" alt="card">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Area End Here -->
</div>

<!-- jquery-->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<!-- Popper js -->
<script src="<?php echo base_url(); ?>assets/js/popper.js"></script>
<!-- Bootstrap js -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- Owl Cauosel JS -->
<script src="<?php echo base_url(); ?>assets/vendor/OwlCarousel/owl.carousel.min.js"></script>
<!-- Meanmenu Js -->
<script src="<?php echo base_url(); ?>assets/js/jquery.meanmenu.min.js"></script>
<!-- Srollup js -->
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollUp.min.js"></script>
<!-- jquery.counterup js -->
<script src="<?php echo base_url(); ?>assets/js/jquery.counterup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/waypoints.min.js"></script>
<!-- Select2 Js -->
<script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>
<!-- Isotope js -->
<script src="<?php echo base_url(); ?>assets/js/isotope.pkgd.min.js"></script>
<!-- Magnific Popup -->
<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>

<!-- Custom Js -->
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
<!-- Validator js -->
<script src="<?= base_url()?>assets/js/validator.min.js"></script>
<!-- jQuery Zoom -->
<script src="<?php echo base_url(); ?>assets/js/jquery.zoom.min.js"></script>
<!-- Google Map js -->
<script src="https://maps.googleapis.com/maps/api/js"></script>

<!--new added -->
<script src="<?php echo base_url(); ?>assets/validation/common.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/address.js"></script>
<script src="<?php echo base_url(); ?>assets/validation/jquery.validate.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/price-filter.js"></script> -->

<!--Strip payment getway-->
<script type="text/javascript" src="https://js.stripe.com/v2/"></script> 

<script>
    $(document).ready(function() {
        $('.loading').hide(); //Default hide in ajax loader.
        get_notification();
    });

    function get_notification(){
        var user_id = $('.login-user-id').val();
        $.ajax({
            url: '<?php echo base_url('notification/count_notification');?>',
            type: "POST",
            data: {user_id:user_id},
            dataType:'json',
            success: function(response) {
                if (response.result == 'success'){
                    $('.count-notification').text(response.count_notification);
                    // $('.get-item-content').html(response.html);
                }
                else if(response.result == 'fail'){
                    $('.count-notification').text(response.count_notification);
                    // $('.get-item-content').html(response.html);
                }
            }
       });
    }

    // User profile complete or not
    $(".post_added").click(function(){
        var country = $(this).data('country');
        var state = $(this).data('state');
        var city = $(this).data('city');

        if (country == 0 || state == 0 || city == 0){
            $.alert({
                title: 'Error',
                type: 'red',
                content: 'You must have to complete profile in order to add post!',
            });
            return false;
        }
        else{
            BASE_URL+'post-add';
        }
    });
    
    // subscribe start //   
    $(".subscribe").click(function(){
        if(isemptyfocus('subscribe_email')){
            return false;
        }
        if(isvalidemail('subscribe_email')){
            return false;
        }
        else{
            var email = $('#subscribe_email').val();
            
            $.ajax({
                url: "<?php echo base_url('login/add_subscribe')?>",
                data: {subscribe_email:email},
                type: "POST",
                dataType:'json',
                
                success: function(response) {
                    if (response.email_allredy) {
                        $.alert({
                            title: 'Error',
                            type: 'red',
                            content: 'Email allredy exist...!',
                        });
                    }
                    if (response.success) {
                        $.alert({
                            title: 'Success',
                            type: 'green',
                            content: 'Subscribe add successfully...!',
                        });
                        $('#subscribe_email').val('');
                    }
                    if (response.error){
                        $.alert({
                            title: 'Error',
                            type: 'red',
                            content: 'Subscribe not added...!',
                        });
                    }
                }
            });
        }
    });

    $('.city_id').on('change', function() {
        var city_id = $(this).val();
        var cat_id = $(this).attr('data-cat-id');
        var sub_cat_id = $(this).attr('data-sub-cat-id');
        var post_data = {'city_id':city_id};
        // alert(city_id);
        $.ajax({
            url: '<?= base_url('home/filter')?>',
            type: 'POST',
            dataType: 'html',
            data: post_data,
            beforeSend: function(){
                $('.loading').show();
            },
            complete: function(){
                $('.loading').hide();
            },
            success: function(response){
                $('.products-grid').html(response);
                //alert(html);
            }, 
        });
    });

    $('.sub-category-id').on('click', function() {
        var sub_cat_id = $(this).attr('data-sub-cat-id');
        var post_data = {'sub_cat_id':sub_cat_id};
        // alert(sub_cat_id);
        $.ajax({
            url: '<?= base_url('home/filter')?>',
            type: 'POST',
            dataType: 'html',
            data: post_data,
            beforeSend: function(){
                $('.loading').show();
            },
            complete: function(){
                $('.loading').hide();
            },
            success: function(response){
                $('.products-grid').html(response);
                //alert(html);
            }, 
        });
    });

    $('.search').on('click', function() {
        if(isemptyfocus('category_name')){
            return false;
        }
        else{
            var category_name = $('#category_name').val();
            var post_data = {'category_name':category_name};
            // alert(city_id);
            $.ajax({
                url: '<?= base_url('home/category_filter')?>',
                type: 'POST',
                dataType: 'html',
                data: post_data,
                beforeSend: function(){
                    $('.loading').show();
                },
                complete: function(){
                    $('.loading').hide();
                },
                success: function(response){
                    $('.default_cat_list').hide();
                    $('.category_list').html(response);
                    //alert(html);
                }, 
            });
        }
    });

    $(document).on('click', '.main-cat', function (e) {
        $(this).next('.collapse').toggleClass('show');
    });

    setInterval(function(){
       get_notification(); // this will run after every 3 seconds
    }, 3000);
</script>
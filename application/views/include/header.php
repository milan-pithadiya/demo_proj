<div class="top_header">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-xs-6">
            <!--<div class="col-md-3 col-xs-6">-->
                <ul class="join_to">
                    <?php
                        $social_media_link = $this->Production_model->get_all_with_where('social_links','','',array());
                        extract($social_media_link[0]);
                    ?>
                    <li><a href="<?=$facebook_link?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="<?=$twiter_link?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="<?=$google_plus_link?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    <li><a href="<?=$instagram_link?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="col-md-offset-4 col-md-6 col-xs-6">
            
            <!--<div class="col-md-offset-4 col-md-5 col-xs-6">-->
                <ul class="phone_ul">
                    <li><span class="fa fa-phone"> +918347850002</span></li>
                    <!--<li><a href="javascript:;"><span class="fa fa-headphones"> Live chat</span></a></li>-->
                    <li><a href="<?=base_url('support')?>"><span class="fa fa-question-circlefa fa-question-circle"> Support</span></a></li>
                    <?php if(!empty($this->session->login_id)){?>
                    
                    <li>
                    <a href="<?=base_url('login/logout')?>" class="btn_login"><span class="fa fa-user"> Logout</span></a></li>
                    <li>
                    <a href="<?=base_url('profile')?>" class="btn_login "><span class="fa fa-user"><?php echo " Hello ".$this->session->username.'!';?> </span></a></li>
                <?php }else{ ?>
                <li>
                    <a href="<?=base_url('login')?>" class="btn_login"><span class="fa fa-user"> Login</span></a></li>
                    <li>
                    <a href="<?=base_url('register')?>" class="btn_login"><span class="fa fa-user"> Register</span></a></li>
            <?php   }?>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Notification <i class="fa fa-bell"></i></a>
                      <ul class="dropdown-menu notify-drop">
                        <!-- end notify title -->
                        <!-- notify content -->
                        <div class="drop-content">
                            <li>
                                <a href="" style="color: #000">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <i class="fa fa-info-circle fa-2x" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-md-10" style="text-align: left;padding: 0;">
                                            You Have 1 Unpaid Invoice(s). Pay Them early peace of mind
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <hr>
                            <li>
                                <a href="" style="color: #000">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <i class="fa fa-info-circle fa-2x" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-md-10" style="text-align: left;padding: 0;">
                                            You Have 1 Unpaid Invoice(s). Pay Them early peace of mind
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <hr>
                            <li>
                                <a href="" style="color: #000">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <i class="fa fa-info-circle fa-2x" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-md-10" style="text-align: left;padding: 0;">
                                            You Have 1 Unpaid Invoice(s). Pay Them early peace of mind
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </div>
                      </ul>
                    </li>
            
                </ul>
            </div>
        </div>
    </div>
</div>
<header>
    <div class="main_header">
        <div class="container">
            <div class="row">
                <div class="col-md-2 text-left col-xs-12">
                    <a href="javascript:;"><img src="<?=base_url()?>assets/image/logo.png" width="130" height="70" class="img_margin"></a>
                    <i class="fa fa-bars"></i>
                </div>
                <div class=" col-md-10">
                    <ul class="nav_bar">
                     <?php if(!empty($this->session->login_id)){?>
                        <!-- <li><a href="<?=base_url()?>">Home</a></li> -->
                        <li><a href="<?=base_url('dashboard')?>">Dashboard</a></li>
                        <!-- <li><a href="<?=base_url('about-us')?>">About Us</a></li>
                        <li><a href="<?=base_url('support')?>">Support</a></li> -->
                        <li><a href="<?=base_url('my-invoices')?>">Billing</a></li>
                        <li menuitemname="Account" class="dropdown" id="Secondary_Navbar-Account" 
                            style="position: relative;">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">  My Plan &nbsp;<b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu" >
                                <li style="display: block;line-height: 5px; ">
                                    <a href="<?=base_url('my-domains')?>" style="font-size: 15px" > My Domain </a>
                                </li>
                                <li style="display: block;line-height: 5px; ">
                                    <a href="<?=base_url('my-hostings')?>" style="font-size: 15px" >  My Hosting </a>
                                </li>
                                <!-- <li style="display: block;line-height: 5px; ">
                                    <a href="<?=base_url('my-domains/register-new-domain')?>" style="font-size: 15px" >  Register New Domain </a>
                                </li> -->
                            </ul>
                        </li>
                        <li><a href="<?=base_url('ticket')?>">Open Ticket</a></li>
                        <li><a href="<?=base_url('domains')?>">Domains</a></li>
                        <!-- <li><a href="<?=base_url('service')?>">Service</a></li> -->
                        <li><a href="<?=base_url('contact')?>">Contact Us</a></li>
                        <li menuitemname="Tutorial" class="dropdown" id="Secondary_Navbar-Tutorial" style="position: relative;">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">  Tutorial &nbsp;<b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu" >
                                <li style="display: block;line-height: 5px; ">
                                    <a href="<?=base_url('tutorials/video-tutorials')?>" style="font-size: 15px" > Video Tutorial </a>
                                </li>
                                <li style="display: block;line-height: 5px; ">
                                    <a href="<?=base_url('tutorials')?>" style="font-size: 15px" >  Tutorial </a>
                                </li>
                            </ul>
                        </li>
                        <li menuitemname="Account" class="dropdown" id="Secondary_Navbar-Account" 
                            style="position: relative;left: 5%;">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">  Account &nbsp;<b class="caret"></b>
                                </a>
                            <ul class="dropdown-menu" >
                                <li menuitemname="Profile" style="display: block;line-height: 5px; ">
                                    <a href="<?=base_url('profile')?>" style="font-size: 15px" >  Show Profile </a>
                                </li>
                                <li menuitemname="Login" style="display: block;line-height: 5px; ">
                                    <a href="<?=base_url('login/logout')?>" style="font-size: 15px" > Logout </a>
                                </li>
                            </ul>
                        </li>
                    <?php } else{?>

                        <li><a href="<?=base_url()?>">Home</a></li>
                        <li><a href="<?=base_url('about-us')?>">About Us</a></li>
                        <li><a href="<?=base_url('hosting-plans')?>">Hosting</a></li>
                        <!-- <li><a href="<?=base_url('support')?>">Support</a></li> -->
                        <li><a href="<?=base_url('domains')?>">Domains</a></li>
                        <!-- <li><a href="<?=base_url('service')?>">Service</a></li> -->
                        <li><a href="<?=base_url('security')?>">Security</a></li>
                        <li><a href="<?=base_url('contact')?>">Contact Us</a></li>

                        <li menuitemname="Account" class="dropdown" id="Secondary_Navbar-Account" 
                            style="position: relative;left: 5%;">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="          true">  Account &nbsp;<b class="caret"></b>
                                </a>
                            <ul class="dropdown-menu" >
                                <li menuitemname="Login" style="display: block;line-height: 5px; ">
                                    <a href="<?=base_url('login')?>" style="font-size: 15px" > Login </a>
                                </li>
                                <li menuitemname="Register" style="display: block;line-height: 5px; ">
                                    <a href="<?=base_url('register')?>" style="font-size: 15px" >  Register </a>
                                </li>
                               
                                <li menuitemname="Forgot Password?" style="display: block;line-height: 5px; ">
                                    <a href="<?=base_url('forgot-password')?>" style="font-size: 15px"> Forgot Password? </a>
                                </li>
                            </ul>
                        </li>
                        <?php }?>
                        <!--<li><i class="fa fa-search"></i></li>
                        <li><i class="fa fa-shopping-cart"></i></li>-->
                    </ul>
                </div>
            </div>
            <div class="search_none">
                <button class="btn_search">Search</button>
                <input type="text" name="" placeholder="search" class="input_search">
            </div>
        </div>
    </div>
</header>   
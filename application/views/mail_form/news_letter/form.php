<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
        <title>Fireneed</title>
        <link rel="icon" type="image/png" sizes="56x56" href="<?= base_url()?>assets/img/favicon.png">
        <style>
            .logo img,header{width:100%}.container,header{display:inline-block}body{text-align:center;font-family:sans-serif;margin:0;padding:0}*{box-sizing:border-box}a,a:hover{color:#fff;text-decoration:none}.logo{float:left;max-width:133px}.view-link{float:right;margin:21px 0}.view-link a{padding:8px 16px;font-size:18px;background-color:#e10a0a}.view-link a:hover{background-color:#272d32}.email-icon,.verify-btn{background-color:#e10a0a}header{padding:20px 0}.container{width:82%;margin:auto}.email-icon{padding:17px 0}.confirmation-box{box-shadow:0 0 10px 1px #848484;padding:0;margin:25px auto 31px;width:80%}.discription{padding:24px 10px}.verify-btn{padding:10px;color:#fff;display:block;width:43%;margin:auto}.verify-btn:hover{background-color:#272d32}.social a{font-size:24px;color:#fff;border-radius:6px;display:inline-block;width:40px;height:40px;margin:0 6px;text-align:center;}.fb{background-color:#3b5998}.twitter{background-color:#55acee}.email{background-color:#fbad1b}@media all and (max-width:736px){.verify-btn{font-size:13px!important}}@media all and (max-width:530px){.logo{display:inline-block;float:none}.view-link{display:block;float:none;margin:36px 0 9px}}
        </style>
    </head>
	<body>
		<header>
			<div class="container">
				<center>
	                <a href="<?= base_url()?>" class="logo">
	                    <?php
		                    $profile_photo = $this->Production_model->get_all_with_where('user','','',array());
		                    if ($profile_photo != null) {
		                    ?>
		                        <img alt="Online-cart" src="<?= $profile_photo !=null ? base_url('assets/uploads/profile_photo/').$profile_photo[0]['profile_photo'] : base_url('assets/uploads/profile_photo/default-image.png')?>">
		                    <?php }
		                ?>
	                </a>
                </center>                
            </div>
		</header>
		
		<div class="container">
			<div class="col-md-12">
				<center>
					<img src="<?= base_url('assets/uploads/news_letter_image/'.$image)?>">
					<br>
					<small><?= $message?></small>
				</center>
			</div>
		</div>
	</body>
</html>
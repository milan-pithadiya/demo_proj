<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/css/footable.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/all.css">
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>User Manage</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User Manage</li>
		</ol>
	</section>
	<section class="content-header">
        <div class="form-group">
            <div class="input-group">
                 <span class="input-group-addon">Search</span>
                 <input type="text" name="search_text" id="search_text" placeholder="Type to Search" class="form-control"  />
            </div>
        </div>
    </section>    
    <section class="content">
    	<?php $this->view('authority/common/messages');?>
        <!-- /.row -->
       <!--  <form class="form_submit" action="<?= base_url('authority/userlist/get_details')?>" method="post" enctype="multipart/form-data"> -->

	        <!-- <div class="row">
	            <div class="col-md-12">
	                <div class="box">
	                    <div class="box-header with-border">
	                        <h3 class="box-title">Mail send</h3>
	                        <div class="box-tools pull-right">
	                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
							</div>
						</div>
	                    <?php
							if (isset($user_details) && $user_details !=null) {
							?>
								<div class="box-body">
									<div class="row">
										<div class="col-md-6">	
											<label>Add Message</label>
											<textarea class="form-control" name="txt_message" id="txt_message"></textarea>
										</div>

										<div class="col-md-4">	
											<label>Send Image</label>
											<input type="file" class="form-control" name="image">
										</div>

										<div class="col-md-2" style="margin-top: 25px;">
											<button type="submit" class="btn btn-warning btn-sm check_mail">Send</button>
										</div>
									</div>
								</div>
							<?php } 
						?>                   
					</div>
				</div>
			</div> -->
			<div class="row">
	            <div class="col-md-12">
	                <div class="box">
	                    <div class="box-header with-border">
	                        <h3 class="box-title">SEARCH</h3>
	                        <div class="box-tools pull-right">
	                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
							</div>
						</div>
	                    <!-- /.box-header -->
	                    <div class="box-body">
	                        <div class="filter-section">
	                           <form action="<?= base_url('authority/userlist/view')?>" method="post" enctype="multipart/form-data">
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
	                                            <label for="filter_by_day">Start Date:</label>
	                                            <input class="form-control filter_date" id="start_date" name="start_date" placeholder="Start Date" type="text" value="<?php if($start_date==""){}else{ echo $start_date;} ?>">
											</div>
										</div>
	                                    <div class="col-md-3">
	                                        <div class="form-group">
	                                            <label for="filter_by_day">End Date:</label>
	                                            <input class="form-control filter_date" id="end_date" name="end_date" placeholder="End Date" type="text" value="<?php if($end_date==""){}else{ echo $end_date;} ?>">
											</div>
										</div>									
										<div class="col-md-2">
											<div class="form-group">
												<label for="filter_by_day">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
												<button type="submit" class="btn btn-default form-control check btn-sm" name="submit" value="submit" formaction="<?= base_url('authority/userlist/view')?>">SEARCH</button>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label for="filter_by_day">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
												<button type="submit" class="btn btn-primary form-control btn-sm" formaction="<?= base_url('authority/userlist/excel_genrate_report')?>">Export Excel</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div> 
					</div>
				</div>
			</div>

			<div class="clearfix"></div>
	        <div class="row">
	            <div class="col-xs-12">
	            	<?php
						if (isset($user_details) && $user_details !=null) {
							?>
								<input type="submit" class="btn btn-sm btn-danger chk_submit" value="Delete all" formaction="<?= base_url('authority/userlist/multiple_delete')?>">
							<?php
						}
					?>
	                <div class="box">	
				      	<div class="box-body table-responsive no-padding">
	                        <table id="myTable" class="table table-bordred table-striped">
	                            <thead>
	                                <tr>
	                                	<th>
											<div class="checkbox" style="margin: -20px 0 0 30px;"><input type="checkbox" name="check_all" id="select_all"></div>
										</th>
	                                    <th>No</th>
	                                    <th>User name</th>
										<th>Email</th>
										<th>Company Name</th>
	                                    <th>Mobile no</th>
	                                    <th>City</th>
	                                    
	                                    <!-- <th>Create Date</th> -->
	                                    <th data-hide="phone,medium"  align="center">Action</th>
										<th data-hide="phone,medium">Status<br/></th>
									</tr>
								</thead>
	                            <tbody>
	                                <?php
										if (isset($user_details) && $user_details !=null):
		                                    foreach ($user_details  as $key => $value) {
		                                    	$id = $value['id'];
											?>
												<tr data-expanded="true">
													<td>
														<div class="checkbox" style="margin-left: 30px"><input type="checkbox" name="chk_multi_checkbox[]" class="chk_all" value="<?= $id?>"><div class="checkbox">
													</td>
													<td><?= $no+$key;?></td>
													<!-- <td>
														<img src="<?= base_url(PROFILE_PICTURE).$value['profile_picture']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">
													</td> -->
													<td><?= $value['first_name']." ".$value['last_name'];?></td>
													<td><?= $value['email'];?></td>
													<td><?= $value['company_name'];?></td>
													<!-- <td>
														<input type="checkbox" name="user_email[]" class="user_email" value="<?= $value['email']?>">
													</td> -->
													<td><?= $value['phone_number'];?></td>

													<td><?=($value['id_city']!=0 )?get_city_by_id($value['id_city']):''?></td>
													<!-- <td>
														<?php 
															if($value['type'] == 'web'){
																echo '<span class="label label-primary">Web</span>';
																
															}
															if($value['type'] == 'android'){
																echo '<span class="label label-warning">Android</span>';
															}
														?>
													</td> -->
													
													<!-- <td><?= date('d-m-Y h:i:s A',strtotime($value['create_date']));?></td> -->
													<td class="action" align="center">

														<p data-placement="top" data-toggle="tooltip" title="Details"><a href="<?php echo site_url(); ?>authority/userlist/details/<?php echo $id; ?>" class="btn btn-primary btn-xs"><span class="fa fa-eye"></span></a></p>
														<!-- <p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo site_url(); ?>authority/userlist/edit/<?php echo $id; ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a></p> -->
														<!-- <p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs delete-btn" data-href="<?php echo site_url(); ?>authority/userlist/delete/<?php echo $id; ?>" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p> -->
														<p data-placement="top" data-toggle="tooltip" title="Delete"><button type="button" class="btn btn-danger btn-xs delete-btn" data-href="<?php echo site_url(); ?>authority/userlist/delete/<?php echo $id; ?>" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p>

														<p data-placement="top" data-toggle="tooltip" title="Reset Password"><button type="button" class="btn btn-warning btn-xs reset-btn" data-href="<?php echo site_url(); ?>authority/userlist/reset_password/<?php echo $value['id']; ?>" data-title="Reset Password" ><i class="fa fa-key" aria-hidden="true"></i></span></button></p>
		                                                <p class="resetpassword">
		                                                    
		                                                </p>
													</td>
													<td>
														<?php 
															if($value['status'] == '1'){
																echo '<span class="label label-success change-status" data-table="user_register" data-id="'.$id.'" data-current-status="1">Active</span>';
																} else {
																echo '<span class="label label-danger change-status" data-table="user_register" data-id="'.$id.'" data-current-status="0">Deactive</span>';
															} 
														?>
													</td>
												</tr>
											<?php
										}
									?>
									<?php else: ?>
									<tr data-expanded="true">
										<td colspan="10" align="center">Records not found</td>
									</tr>
									<?php endif; ?>
								</tbody>
							</table>
						</div>
						<div class="row">
	                        <div class="col-md-12" style="padding: 0px 30px 0px 0;">
	                            <ul class="pagination pull-right">
	                                <?php
	                                    if (isset($pagination)) 
	                                    { 
	                                        echo $pagination;
	                                    }
	                                ?>
	                            </ul>
	                        </div>
	                    </div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
			</div>
		<!-- </form> -->
	</section>
</div>
<div id="resetpasswordform">
    <form  method="post" id="reset_password_link">
        <input type="text" name="password" id="password" value="">
        <button type="submit" class="btn btn-success btn-xs checkp" data-toggle="tooltip" title="Lock Password"><span class="glyphicon glyphicon-lock"></span> </button>
    </form>
</div>
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
				<h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" value="" name="delete_link" id="delete_link"/>
				<div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
			</div>
			<div class="modal-footer ">
				<button type="button" class="btn btn-success btn-confirm-yes" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
				<button type="button" class="btn btn-default btn-confirm-no" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
			</div>
		</div>
		<!-- /.modal-content --> 
	</div>
	<!-- /.modal-dialog --> 
</div>
<!-- DELETE POPUP -->
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/js/footable.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript">
	$(document).ready(function () {	
		$(document).on('click','.change-status',function(){
			var current_element = jQuery(this);
			var id = jQuery(this).data('id');
			var table = jQuery(this).data('table');
			var current_status = jQuery(this).attr('data-current-status');
			// alert(current_status);
			var post_data = {
				'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
				'action': 'change_status',
				'id': id,
				'table': table,
				'current_status': current_status,
			}
			$.ajax({
				type: "POST",
				url: BASE_URL + 'authority/ajax/change_status',
				data: post_data,
				async: false,
				success: function (response) {
					var response = JSON.parse(response);
					if (response.success) {
						current_element.toggleClass('label-danger label-success');
						if(current_element.hasClass('label-success')){
							current_element.text('Active');
							current_element.attr('data-current-status','1');
							} else {
							current_element.text('Deactive');
							current_element.attr('data-current-status','0');
						}
						} else {
						window.location = window.location.href;
					}
				}
			});
		});
		$(document).on("click", ".delete-btn", function () {
			$("#delete_link").val($(this).data("href"));
		});
		$(".btn-confirm-yes").on("click", function () {
			window.location = $("#delete_link").val();
		});
	});
	$('.check_mail').click(function(){
        if(isemptyfocus('txt_message'))
        {
            return false;
        }
        var email = $('.user_email:checkbox');
        if(email.length > 0) {
            if($('.user_email:checkbox:checked').length < 1) {
                $.alert({
                	type: 'red',
			        title: 'Confirm Delete',
			        content: 'Please select at least one user',
			    });
                return false;
            }
        }
    });

	$('.check').click(function(){
        if(isemptyfocus('start_date') || isemptyfocus('end_date'))
        {
            return false;
        }
    });
    // multiple delete //
    $('.chk_submit').on('click', function() {
		var boxes = $('.chk_all:checkbox');
        if(boxes.length > 0) {
            if($('.chk_all:checkbox:checked').length < 1) {
                $.alert({
			        title: 'Confirm Delete',
			        content: 'Please select at least one checkbox',
			    });
                return false;
            }
            else{
        		confirm('Are you sure you want to delete this item?');
	        	return true;
            }
        }
	});
</script>

 <script type="text/javascript">

 		$(document).on("click", ".reset-btn", function () {
            // $(this).parent("p").('#resetpasswordform').remove();
            $(this).closest('p').find('#resetpasswordform').toggle();
            $(this).parent("p").html($("#resetpasswordform").html());
            $("#resetpasswordform").css('position','absolute');
            $("#reset_password_link").attr('action',$(this).data("href"));

        });


       $(document).on("submit", "#reset_password_link", function () {
        var password = $("#password").val();          
        if (password ==''){
            $('#password').css('border','1px solid red');
            $('#password').focus();
            return false;
        }
        
    });
 </script>
<?php $this->view('authority/common/footer'); ?>									
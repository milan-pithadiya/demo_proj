<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/css/jquery-ui-timepicker-addon.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/css/select2.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url() . "authority/user/view"; ?>"> User</a></li>
        </ol>
    </section>

    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?php
                $attributes = array("id" => "form", "name" => "form", "method" => "POST", "enctype" => "multipart/form-data");
                echo form_open("", $attributes);
                ?>
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $form_title; ?></h3>

                        <div class="box-tools pull-right">
                            <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>-->
                            <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php
                        if (isset($success)) {
                            ?>
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-check"></i> Success</h4>
                                <?php echo $success; ?>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="form-group">
                            <label for="full_name">Name:<span class="required">*</span></label>
                            <input class="form-control" name="full_name" id="full_name" placeholder="Enter full name" type="text" value="<?php echo (isset($full_name) ? $full_name : ""); ?>">
                            <?php echo form_error("full_name", "<div class='error'>", "</div>"); ?>
                        </div>
                        <div class="form-group">
                            <label for="email_address">Email Address:<span class="required">*</span></label>
                            <input class="form-control" name="email_address" id="email_address" placeholder="Enter email addres" type="email" value="<?php echo (isset($email_address) ? $email_address : ""); ?>">
                            <?php echo form_error("email_address", "<div class='error'>", "</div>"); ?>
                        </div>
                        <div class="form-group">
                            <label for="mobile_number_1">Mobile Number 1:<span class="required">*</span></label>
                            <input class="form-control" name="mobile_number_1" id="mobile_number_1" placeholder="Enter mobile number" type="text" minlength=10 maxlength=25 value="<?php echo (isset($mobile_number_1) ? $mobile_number_1 : ""); ?>">
                            <?php echo form_error("mobile_number_1", "<div class='error'>", "</div>"); ?>
                        </div>
                        <div class="form-group">
                            <label for="mobile_number_2">Mobile Number 2:</label>
                            <input class="form-control" name="mobile_number_2" id="mobile_number_2" placeholder="Enter mobile number" type="text" minlength=10 maxlength=25 value="<?php echo (isset($mobile_number_2) ? $mobile_number_2 : ""); ?>">
                            <?php echo form_error("mobile_number_2", "<div class='error'>", "</div>"); ?>
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <textarea class="form-control" name="address" id="address"><?php echo (isset($address) ? $address: ""); ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="address">Description</label>
                            <textarea name="description" id="editor1" class="form-control" rows="3" cols="80"><?php echo (isset($description) ? $description: ""); ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="gender">Gender:<span class="required">*</span></label>
                            <?php
                            $gender_info = array("Male", "Female", "Transgender");
                            ?>
                            <select class="form-control" name="gender" id="gender">
                                <?php
                                if (isset($gender_info) && !empty($gender_info)) {
                                    $selected = (isset($gender) ? $gender : "");
                                    foreach ($gender_info as $value) {
                                        $selected_text = "";
                                        if ($value == $selected) {
                                            $selected_text = "selected='selected'";
                                        }
                                        echo "<option value='" . $value . "' " . $selected_text . ">" . $value . "</option>";
                                    }
                                }
                                ?>
                            </select>
                            <?php echo form_error("gender", "<div class='error'>", "</div>"); ?>
                        </div>                        
                        <div class="form-group">
                            <label for="profile_photo">Profile Photo:<span class="required">*</span>(Upload by 1200&#215;300)</label>
                            <input type="file" name="profile_photo" id="profile_photo" />
                            <?php echo form_error("profile_photo", "<div class='error'>", "</div>"); ?>
                            <?php
                            if (isset($profile_photo_error) && $profile_photo_error != "") {
                                echo "<div class='error'>" . $profile_photo_error . "</div>";
                            }
                            ?>
                        </div>
                        <?php
                            $profile_photo = $this->Production_model->get_all_with_where('user','','',array('id'=>isset($id)?$id:''));

                            if ($profile_photo != null) {
                            ?>
                                <!-- <div class="form-group col-md-6 row">
                                    <label>Current Image</label><br>
                                    <img src="<?//= $profile_photo !=null ? base_url('assets/uploads/profile_photo/').$profile_photo[0]['footer_logo'] : base_url('assets/uploads/profile_photo/default-image.png')?>" onerror="this.src='<?//= base_url('assets/uploads/default_img.png')?>'" height="50" wodth="200">
                                </div> -->

                                <div class="form-group">
                                    <label>Current Photo</label><br>
                                    <div style="height: 40px;width: 100px;padding: 0 0 0 8px;">
                                        <img src="<?= $profile_photo !=null ? base_url(PROFILE_PHOTO).$profile_photo[0]['profile_photo'] : base_url(PROFILE_PHOTO.'default-image.png')?>"  height="50" wodth="200">
                                    </div>
                                </div>
                            <?php }
                        ?><br>
                        <div class="form-group">
                            <input class="btn btn-success text-uppercase" value="Submit" type="submit">                            
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- /.box -->
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery.ui.touch-punch.min.js"></script>
<script>
    /*FORM VALIDATION*/
    $("#form1").validate({
        rules: {
            full_name: "required",
            email_address: {required: true, email: true},
            mobile_number_1: {required: true, minlength: 10}, //maxlength: 15,
            mobile_number_2: {minlength: 10}, //maxlength: 15,
        },
        messages: {
            full_name: "Please enter full name",
            email_address: {required: "Please enter email address", email: "Please enter valid email address"},
            mobile_number_1: {required: "Please enter mobile number", maxlength: "Please enter no more than 15 digits", minlength: "Please enter at least 10 digits"},

            mobile_number_2: {maxlength: "Please enter no more than 15 digits", minlength: "Please enter at least 10 digits"},
        }
    });
</script>
<?php $this->view('authority/common/footer'); ?>
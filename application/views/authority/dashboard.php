<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/css/jquery-ui-timepicker-addon.css"/>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Dashboard</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">                
                    <h3><?= $count_slider !=null ? $count_slider :0;?></h3>                
                    <p>Slider</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo base_url(); ?>authority/home_slider/view" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">                
                    <h3><?= $count_reg !=null ? $count_reg :0;?></h3>                 
                    <p>Users</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo base_url(); ?>authority/userlist/view" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-blue">
                <div class="inner">                
                    <h3><?= $count_sub_category !=null ? $count_sub_category :0;?></h3>                
                    <p>Sub Category</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo base_url(); ?>authority/subcategory" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div> -->

        <!-- <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">                
                    <h3><?= $count_product !=null ? $count_product :0;?></h3>                
                    <p>Product</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo base_url(); ?>authority/product_manage" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div> -->
        <!-- <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">                
                    <h3><?= $count_contact !=null ? $count_contact :0;?></h3>                
                    <p>Contact Us</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo base_url(); ?>authority/contact_us/view" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div> -->
        <!-- <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">                
                    <h3><?= $count_subscribe !=null ? $count_subscribe :0;?></h3>                
                    <p>Subscribe List</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo base_url(); ?>authority/subscribe/view" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div> -->
        <!-- <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">                
                    <h3><?= $count_reg !=null ? $count_reg :0;?></h3>                
                    <p>Register</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo base_url(); ?>authority/userlist/view" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div> -->
        <!-- <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-blue">
                <div class="inner">                
                    <h3><?= $count_feedback !=null ? $count_feedback :0;?></h3>                
                    <p>Feedback</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo base_url(); ?>authority/feedback/view" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div> -->
    </section>
    <!-- /.content -->
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery.ui.touch-punch.min.js"></script>
<?php $this->view('authority/common/footer'); ?>
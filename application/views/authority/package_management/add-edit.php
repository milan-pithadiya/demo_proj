<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/css/jquery-ui-timepicker-addon.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/css/select2.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home </a></li>
            <li><a href="<?php echo site_url() . "authority/package-management/view"; ?>">Package Management</a></li>
            <!-- <li class="active">Add</li> -->
        </ol>
    </section>

    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <?php $action = ($package_data == null) ? base_url('authority/package-management/add_package') : base_url('authority/package-management/update_package')?>

                    <form id="form" action="<?= $action ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id" id="package_id" value="<?= ($package_data != null) ? $package_data[0]['id'] : '';?>">

                        <div class="box-header with-border">
                            <h3 class="box-title"><?= $package_data == null ? 'Add' : 'Edit'?> Package</h3>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <?php $this->load->view('authority/common/messages')?>
                            <div class="form-group col-md-6">
                                <label for="full_name">Name :<span class="required">*</span></label>
                                <input type="text" name="name" class="form-control name" placeholder="Name" value="<?= ($package_data != null) ? $package_data[0]['name'] : '';?>">
                                <span class="error_name" style="color: #fc3a3a;"></span>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="full_name">Price :<span class="required">*</span></label>
                                <input type="text" name="price" class="form-control digits price" placeholder="Price" value="<?= ($package_data != null) ? $package_data[0]['price'] : '';?>" maxlength="5">
                                <span class="error_price" style="color: #fc3a3a;"></span>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="full_name">Currency :<span class="required">*</span></label>
                                <select class="form-control currency" name="currency">
                                    <option value="">Select</option>
                                    <option value="INR" <?= $package_data !=null && $package_data[0]['currency'] == 'INR' ? 'selected' : '';?>>Rs</option>
                                    <!-- <option value="USD" <?= $package_data !=null && $package_data[0]['currency'] == 'USD' ? 'selected' : '';?>>$</option>
                                    <option value="EUR" <?= $package_data !=null && $package_data[0]['currency'] == 'EUR' ? 'selected' : '';?>>€</option>
                                    <option value="GBP" <?= $package_data !=null && $package_data[0]['currency'] == 'GBP' ? 'selected' : '';?>>£</option> -->
                                </select>
                                <span class="error_currency" style="color: #fc3a3a;"></span>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="full_name">Position :</label>
                                <input type="text" name="position" class="form-control only_digits" placeholder="Position" value="<?= ($package_data != null) ? $package_data[0]['position'] : '';?>" maxlength="5">
                            </div>

                            <div class="form-group col-md-3">
                                <label for="full_name">Day/Month :<span class="required">*</span></label>
                                <select class="form-control day_month" name="day_month">
                                    <option value="">Select</option>
                                    <option value="Day" <?= $package_data !=null && $package_data[0]['day_month'] == 'Day' ? 'selected' : '';?>>Day</option>
                                    <option value="Month" <?= $package_data !=null && $package_data[0]['day_month'] == 'Month' ? 'selected' : '';?>>Month</option>
                                </select>
                                <span class="error_day_month" style="color: #fc3a3a;"></span>
                            </div>
                            <div class="form-group col-md-3 day_select">
                                <label for="full_name">Day :<span class="required">*</span></label>
                                <input type="text" name="day" class="form-control only_digits day" placeholder="Day" value="<?= ($package_data != null) ? $package_data[0]['day'] : '';?>" maxlength="5">
                                <span class="error_day" style="color: #fc3a3a;"></span>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="last_name">Description :<span class="required">*</span></label>
                                <textarea name="discription" id="editor1" class="form-control discription" placeholder="Description"><?= ($package_data != null) ? $package_data[0]['discription'] : '';?></textarea>
                                <span class="error_desc" style="color: #fc3a3a;"></span>
                            </div>

                            <div class="form-group col-md-12">
                                <input type="submit" class="btn btn-success text-uppercase check" value="Submit">
                                <a href="<?= base_url('authority/package-management/view')?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery.ui.touch-punch.min.js"></script>

<script>
	$(document).ready(function(){
        $('.day_select').hide();

        var day_month = $('.day_month').val();
        if (day_month == 'Day'){
            $('.day_select').show();            
        }

		/*FORM VALIDATION*/
		// $("#form").validate({
		// 	rules: {
		// 		user_name: "required",            
		// 		last_name: "required",
		// 		email_phone: 'required',
		// 	},
		// 	messages: {
		// 		user_name: "Please enter first name",
		// 		last_name: "Please enter last name",
		// 		email_phone: 'Please enter email address or phone number',
		// 	}
		// });
        
        $(".day_month").change(function(){
            var day_month = $(this).val();
            // alert(day_month);
            if (day_month == 'Day'){
                $('.day_select').show();
                if (day ==''){
                    $('.error_day').text('Please enter package day.');
                    $('.day').focus();
                    return false;
                }
            }
            if (day_month == 'Month'){
                $('.day_select').hide();
            }
        });

        $('.check').click(function(){

            var name = $(".name").val();
            var price = $(".price").val();
            var currency = $(".currency").val();
            var day_month = $(".day_month").val();
            var day = $(".day").val();
            var nicInstance = nicEditors.findEditor('editor1');
            var dis = nicInstance.getContent();
            // var about_image = $(".about_image")[0].files.length;
            if (name ==''){
                $('.error_name').text('Please enter package name.');
                $('.name').focus();
                return false;
            }
            if (price ==''){
                $('.error_price').text('Please enter price.');
                $('.price').focus();
                return false;
            }
            if (currency ==''){
                $('.error_currency').text('Please select currency.');
                $('.currency').focus();
                return false;
            }
            if (day_month ==''){
                $('.error_day_month').text('Please  select day_month.');
                $('.day_month').focus();
                return false;
            }
            if (day_month == 'Day'){
                if (day ==''){
                    $('.error_day').text('Please enter package day.');
                    $('.day').focus();
                    return false;
                }
            }

            if (dis =="<br>"){
                $('.error_desc').text('Please enter description.');
                return false;
            }

            // if (about_id == ''){
            //     if(about_image === 0){
            //         $('.error_file').text("Please select image.");
            //         return false;
            //     }
            // }
        });
	})

</script>
<?php $this->view('authority/common/footer'); ?>
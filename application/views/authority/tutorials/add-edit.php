<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Tutorial Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Tutorial Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php echo site_url() . "authority/tutorials/view"; ?>">Tutorial</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $tutorial_details !=null ? 'Edit' : 'Add'?> Tutorial</h3>

                        <!-- <form method="post" action="<?//= base_url('authority/tutorials/save_excel_tutorial')?>" enctype="multipart/form-data"><br>                            
                            <div class="col-md-4"> 
                                <input type="file" name="xls_file" id="xls_file" class="form-control" accept=".xlsx, .xls">
                            </div>
                            <div class="col-md-2"> 
                                <input type="submit" class="btn btn-sm btn-success check_excel" value="submit">
                            </div>
                            <div class="col-md-4">                            
                                <a href="<?//= base_url('authority/tutorials/download_excel_tutorial');?>" class="btn btn-sm btn-warning">Excel Download</a>
                            </div>
                        </form>
                        <div class="col-md-12" style="color: red; font-size: 20px;"><center>(OR)</center></div> -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php $this->load->view('authority/common/messages');?>
                        <?php $action = $tutorial_details !=null ? base_url('authority/tutorials/update_tutorial') : base_url('authority/tutorials/insert_tutorial'); ?>

                        <form id="form" method="post" action="<?= $action?>" enctype="multipart/form-data">
                            <div class="clone-section-main">   
                                <div class="clone-section-sub">   
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <input type="hidden" name="id" id="tutorial_id" value="<?= ($tutorial_details !=null) ? $tutorial_details[0]['id'] : ""; ?>">
                                            <label for="tutorial_name">Tutorial name :<span class="required">*</span></label> 
                                            <?php
                                                $input_fields = array(
                                                    'name' => 'tutorial_name[]',
                                                    'placeholder' => 'Tutorial name',
                                                    'class' => 'form-control tutorial_name txtonly',
                                                    'id' => 'tutorial_name',
                                                    'value' => (isset($tutorial_details) && $tutorial_details !=null ? $tutorial_details[0]['tutorial_name'] : ""),
                                                );
                                                echo form_input($input_fields);
                                                echo form_error("tutorial_name", "<div class='error'>", "</div>");
                                            ?>
                                            <span class="error_tutorial_name" style="color: #fc3a3a;"></span>
                                        </div>
                                        <div class="col-md-2">
                                            <?php
                                                if ($tutorial_details == null) {
                                                    ?>   
                                                        <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                        <button type="button" class="btn add-more pull-left" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                        
                                                        <button type="button" class="btn remove-more btn-danger pull-right" title="Delete"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                    <?php 
                                                }
                                            ?>
                                        </div>
                                        <br><br><br><br>
                                        <div class="col-md-10">
                                            <label for="tutorial_link">Tutorial Link :<span class="required">*</span></label> 
                                            <?php
                                                $input_fields = array(
                                                    'name' => 'tutorial_link[]',
                                                    'placeholder' => 'Tutorial Link',
                                                    'class' => 'form-control tutorial_link txtonly',
                                                    'id' => 'tutorial_link',
                                                    'value' => (isset($tutorial_details) && $tutorial_details !=null ? $tutorial_details[0]['tutorial_link'] : ""),
                                                );
                                                echo form_input($input_fields);
                                                echo form_error("tutorial_link", "<div class='error'>", "</div>");
                                            ?>
                                            <span class="error_tutorial_link" style="color: #fc3a3a;"></span>
                                        </div>
                                    </div><br><br><br><br>
                                    <!-- <div class="col-md-4">
                                        <label for="cat_icon">Tutorial Icon :
                                            <span class="required">*</span>(Upload by 60&#215;60)
                                        </label>

                                        <input type="file" name="cat_icon" class="form-control cat_icon" class="form-control cat_icon" accept="image/*">
                                        <span class="error_icon" style="color: #fc3a3a;"></span>
                                    </div> -->
                                    
                                    <!-- <div class="form-group">
                                        <div class="col-md-10">
                                            <label for="cat_image">Tutorial Image :(Upload by 30&#215;30)
                                            </label>

                                            <input type="file" name="cat_image[]" class="form-control cat_image" class="form-control cat_image" accept="image/*">
                                            <span class="error_file" style="color: #fc3a3a;"></span>
                                        </div>
                                        
                                    </div> -->
                                </div>
                            </div>

                            <!-- <?php
                                if ($tutorial_details != null) {
                                    ?>
                                    <div class="form-group col-md-10">
                                        <label for="last_name">Current image</label><br>
                                        
                                        <img src="<?= base_url(CAT_IMAGE).$tutorial_details[0]['cat_image']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">
                                    </div>
                                <?php }
                            ?> -->
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input class="btn btn-success text-uppercase check" value="Submit" type="submit">
                                    <a href="<?php echo site_url() . 'authority/tutorial'; ?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </section>
    </div>
    <?php $this->view('authority/common/copyright'); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
    <script>
        /*FORM VALIDATION*/
        var tutorial_id = $("#tutorial_id").val();
        $("#form").validate({
            rules: {
                'tutorial_name[]': {required: true},      
                // 'cat_image[]': { 
                //     required: function(element) {
                //         if (tutorial_id == '') {  
                //             return true;
                //         }
                //         else {
                //             return false;
                //         }
                //     }, 
                // },                  
            },
            messages: {
                'tutorial_name[]': "Please enter tutorial name",
                // 'cat_image[]': "Please select image",       
            }
        });

        // $('.check').click(function(){
        //     var tutorial_id = $("#tutorial_id").val();

        //     var tutorial_name = $(".tutorial_name_english").val();
        //     var cat_image = $(".cat_image")[0].files.length;

        //     if (tutorial_name_english =='' && tutorial_name_arabic ==''){
        //         $('.error_tutorial_name').text('Please enter english OR arabic tutorial-name.');
        //         $('.tutorial_name_arabic').focus();
        //         return false;
        //     }  

        //     if (tutorial_id == ''){
        //         if(cat_image === 0){
        //             $('.error_file').text("Please select image.");
        //             return false;
        //         }
        //     } 
        // });

        $('.check_excel').click(function(){
            if(isemptyfocus('xls_file'))
            {
                return false;
            }
        });

        $(document).on('click', '.add-more', function() {
            $clone = $('.clone-section-sub:last').clone();
            $('.clone-section-main').append($clone);
            $('.clone-section-sub:last').find('input[type="text"]').val('');
            $('.clone-section-sub:last').find('input[type="file"]').val('');
        });

        $(document).on('click', '.remove-more', function () {
            if (check_clone_lang_section()) {
                $(this).closest('.clone-section-sub').remove();
            } else {
                alert('You can not remove the current section');
            }
        });

        function check_clone_lang_section() {
            if ($('.clone-section-main').find('.clone-section-sub').length > 1) {
                return true;
            } else {
                return false;
            }
        } 
    </script>
<?php $this->view('authority/common/footer'); ?>
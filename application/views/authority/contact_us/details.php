<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Contact us</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Contact us</li>
        </ol>
    </section>
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table id="mytable" class="table table-bordred">
                            <tbody>
                                <tr>
                                    <td><b>User name:</b></td>
                                    <td><?= $name;?></td>
                                </tr>
								<tr>
                                    <td><b>Email:</b></td>
                                    <td><?= $email; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Subject:</b></td>
                                    <td><?= $subject; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Phone No:</b></td>
                                    <td><?= $phone_number; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Message:</b></td>
                                    <td><?= $message; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Create date:</b></td>
                                    <td><?= date('d-m-Y h:i:s A',strtotime($create_date)); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<?php $this->view('authority/common/footer'); ?>
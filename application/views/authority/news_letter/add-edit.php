<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/css/jquery-ui-timepicker-addon.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/css/select2.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home </a></li>
            <li><a href="<?php echo site_url() . "authority/news_letter/view"; ?>">News letter </a></li>
            <!-- <li class="active">Add</li> -->
        </ol>
    </section>

    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <?php $action = ($news_letter_data == null) ? base_url('authority/news_letter/add_news_letter') : base_url('authority/news_letter/update_news_letter')?>

                    <form id="form" action="<?= $action ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id" id="news_letter_id" value="<?= ($news_letter_data != null) ? $news_letter_data[0]['id'] : '';?>">

                        <div class="box-header with-border">
                            <h3 class="box-title"><?= $news_letter_data == null ? 'Add' : 'Edit'?> News letter</h3>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <?php $this->load->view('authority/common/messages')?>
                            <div class="form-group">
                                <label for="name">Name :<span class="required">*</span> (Configure your server name add)</label>
                                <input type="text" name="name" class="form-control name" placeholder="Name" value="<?= ($news_letter_data != null) ? $news_letter_data[0]['name'] : '';?>">
                                <span class="error_name" style="color: #fc3a3a;"></span>
                            </div>
                            
                            <div class="form-group">
                                <label for="email">Email :<span class="required">*</span> (Configure your server email address add , other wise not send in email)</label>
                                <input type="text" name="email" class="form-control email" id="user_email" placeholder="Email" value="<?= ($news_letter_data != null) ? $news_letter_data[0]['email'] : '';?>">
                                <div id="valid" style="color: #F66249"></div>
                                <span class="error_email" style="color: #fc3a3a;"></span>
                            </div>

                            <div class="form-group">
                                <label for="last_name">Subject :<span class="required">*</span></label>
                                <input type="text" name="subject" class="form-control subject" placeholder="Subject" value="<?= ($news_letter_data != null) ? $news_letter_data[0]['subject'] : '';?>">
                                <span class="error_sub" style="color: #fc3a3a;"></span>
                            </div>

                            <div class="form-group">
                                <label for="last_name">Message :<span class="required">*</span></label>
                                <textarea name="message" class="form-control message" id="editor1" placeholder="Message"><?= ($news_letter_data != null) ? $news_letter_data[0]['message'] : '';?></textarea>
                                <span class="error_msg" style="color: #fc3a3a;"></span>
                            </div>    

    						<div class="form-group">
                                <label for="last_name">Image:<span class="required">*</span></label>
    							<input type="file" name="news_letter_image" class="form-control news_letter_image" accept="image/*">	
                                <span class="error_file" style="color: #fc3a3a;"></span>
                            </div>   

                            <?php
                                if ($news_letter_data != null) {
                                ?>
                                    <div class="form-group">
                                        <label for="last_name">Current image:<span class="required">*</span></label><br>
                                        <img src="<?= base_url(NEWS_LETTER_IMAGE).$news_letter_data[0]['image']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">
                                    </div>
                                <?php }
                            ?>

                            <div class="form-group">
                                <input type="submit" class="btn btn-success text-uppercase check" value="Submit">
                                <a href="<?= base_url('authority/news_letter/view')?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery.ui.touch-punch.min.js"></script>

<script>
	$(document).ready(function(){
		/*FORM VALIDATION*/
		// $("#form").validate({
		// 	rules: {
		// 		user_name: "required",            
		// 		last_name: "required",
		// 		email_phone: 'required',
		// 	},
		// 	messages: {
		// 		user_name: "Please enter first name",
		// 		last_name: "Please enter last name",
		// 		email_phone: 'Please enter email address or phone number',
		// 	}
		// });
        
        $('.check').click(function(){
            var news_letter_id = $("#news_letter_id").val();

            var name = $(".name").val();
            var email = $(".email").val();            
            var subject = $(".subject").val();
            var news_letter_image = $(".news_letter_image")[0].files.length;
           
            var nicInstance = nicEditors.findEditor('editor1');
            var messageContent = nicInstance.getContent();

            if (name ==''){
                $('.error_name').text('Please enter name.');
                $('.name').focus();
                return false;
            }
            if (email ==''){
                $('.error_email').text('Please enter email.');
                $('.email').focus();
                return false;
            }
            if(isvalidemail('user_email')){
                return false;
            }
            if (subject ==''){
                $('.error_sub').text('Please enter subject.');
                $('.subject').focus();
                return false;
            }  
            if(messageContent=="<br>") { 
                alert("Please enter the value message");
                return false;
            }
            if (news_letter_id == ''){
                if(news_letter_image === 0){
                    $('.error_file').text("Please select image.");
                    return false;
                }
            }
        });
	})
</script>
<?php $this->view('authority/common/footer'); ?>
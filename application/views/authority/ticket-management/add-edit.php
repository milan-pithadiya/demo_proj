<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<?php $this->view('authority/common/sidebar'); ?>

<div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <h3><?= $form_title?></h3>
        <ul>
            <li><a href="<?= base_url('authority/dashboard')?>">Home</a></li>
            <li><?= $form_title?></li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Admit Form Area Start Here -->
    <div class="card height-auto">
        <?php $action = $ticket_details !=null ? base_url('authority/ticket_management/update_ticket') : base_url('authority/ticket_management/add_ticket'); ?>
        <form class="new-added-form" action="<?= $action?>" id="form" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?= isset($ticket_details) && $ticket_details !=null ? $ticket_details[0]['id'] : ''?>">
                    
            <div class="card-body">                   
                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Date *</label>
                        <input type="text" placeholder="Date" name="create_date" class="form-control start_date" data-position='bottom right' value="<?= isset($ticket_details) && $ticket_details !=null ? date('d-m-Y',strtotime($ticket_details[0]['create_date'])) : date('d-m-Y');?>">
                        <i class="far fa-calendar-alt" style="top: 50px;"></i>
                        <div id="create_date_validate"></div>
                    </div>                    

                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Time *</label>
                        <input type="text" name="time" id="timepicker1" placeholder="Time" class="form-control time" value="<?= isset($ticket_details) && $ticket_details !=null ? $ticket_details[0]['time'] : date("h:i:s A", strtotime("now"));?>">
                        <div id="time_validate"></div>
                    </div>

                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Ticket No *</label>
                        <input type="text" name="ticket_no" placeholder="Ticket No" class="form-control only_digits" value="<?= isset($ticket_details) && $ticket_details !=null ? $ticket_details[0]['ticket_no'] : mt_rand(10000000, 99999999);?>" readonly="readonly">
                        <?php echo form_error("ticket_no", "<div class='error'>", "</div>"); ?>
                    </div>
                    
                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Ticket Status *</label>
                        <select class="select2" name="ticket_status_id">
                            <option value="">Please Select Status *</option>
                            <?php
                                if (isset($ticket_status_details) && $ticket_status_details !=null) {
                                    foreach ($ticket_status_details as $key => $value) {
                                    ?>
                                        <option value="<?= $value['id']?>" <?= isset($ticket_details) && $ticket_details !=null && $ticket_details[0]['ticket_status_id'] == $value['id'] ? 'selected' : '';?>><?= $value['status_name']?></option>
                                    <?php }
                                }
                            ?>
                        </select>
                        <div id="ticket_status_id_validate"></div>
                    </div>

                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Location *</label>
                        <select class="select2" name="ticket_location_id">
                            <option value="">Please Select Location *</option>
                            <?php
                                if (isset($ticket_location_details) && $ticket_location_details !=null) {
                                    foreach ($ticket_location_details as $key => $value) {
                                    ?>
                                        <option value="<?= $value['id']?>" <?= isset($ticket_details) && $ticket_details !=null && $value['id'] == $ticket_details[0]['ticket_location_id'] ? 'selected' : '';?>><?= $value['location_name']?></option>
                                    <?php }
                                }
                            ?>
                        </select>
                        <div id="ticket_location_id_validate"></div>
                        <?php echo form_error("ticket_location_id", "<div class='error'>", "</div>"); ?>
                    </div>

                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Preferred Date *</label>
                        <input type="text" placeholder="Preferred Date" id="preferred_date" name="preferred_date" class="form-control start_date" data-position='bottom right' value="<?= isset($ticket_details) && $ticket_details !=null ? date('d-m-Y',strtotime($ticket_details[0]['preferred_date'])) : date('d-m-Y');?>">
                        <i class="far fa-calendar-alt" style="top: 50px;"></i>
                        <div id="preferred_date_validate"></div>
                        <?php echo form_error("preferred_date", "<div class='error'>", "</div>"); ?>
                    </div>

                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Preferred Time *</label>
                        <input type="text" name="preferred_time" id="timepicker2" placeholder="Preferred Time" class="form-control" value="<?= isset($ticket_details) && $ticket_details !=null ? $ticket_details[0]['preferred_time'] : date("h:i:s A", strtotime("now"));?>">
                        <div id="preferred_time_validate"></div>
                        <?php echo form_error("preferred_time", "<div class='error'>", "</div>"); ?>
                    </div>

                    <div class="col-xl-3 col-lg-6 col-12 form-group">
                        <label>Priority *</label>
                        <select class="select2" name="priority_id">
                            <option value="">Please Select Priority *</option>
                            <?php
                                if (isset($priority_details) && $priority_details !=null) {
                                    foreach ($priority_details as $key => $value) {
                                    ?>
                                        <option value="<?= $value['id']?>" <?= isset($ticket_details) && $ticket_details !=null && $ticket_details[0]['priority_id'] == $value['id'] ? 'selected' : '';?>><?= $value['priority_name']?></option>
                                    <?php } 
                                }
                            ?>
                        </select>
                        <div id="priority_id_validate"></div>
                        <?php echo form_error("priority_id", "<div class='error'>", "</div>"); ?>
                    </div>

                    <div class="col-xl-12 col-lg-6 col-12 form-group">
                        <label>Problems *</label>
                        <?php
                            if (isset($ticket_problem_details) && $ticket_problem_details !=null) {
                                $ticket_problem = isset($ticket_details[0]['ticket_problem_id']) && $ticket_details[0]['ticket_problem_id'] !=null ? array_map('trim', explode(",", $ticket_details[0]['ticket_problem_id'])) : '';

                                $ticket_problem = isset($ticket_problem) && $ticket_problem !=null ? $ticket_problem : array();

                                // echo"<pre>"; print_r($problem); 
                                foreach ($ticket_problem_details as $key => $value) {
                                    $id = $value['id'];
                                ?>
                                    <div class="form-check col-xl-3" style="float: left;">
                                        <input type="checkbox" name="ticket_problem_id[]" id="test<?= $key+1?>" class="form-check-input chk_all" value="<?= $id?>" <?=(in_array($id, $ticket_problem)?'checked="checked"': null)?>>
                                        <label class="form-check-label" for="test<?= $key+1?>"><?= $value['problem_name']?></label>
                                    </div>
                                <?php }
                            }
                        ?>
                        <div id="ticket_problem_id"></div>
                    </div>

                    <div class="col-lg-12 col-12 form-group">
                        <label>Problem</label>
                        <textarea class="textarea form-control" name="problem" id="editor1" cols="10"
                            rows="9"><?php echo (isset($problem) ? $problem: ""); ?><?= isset($ticket_details) && $ticket_details !=null ? $ticket_details[0]['problem'] : '';?></textarea>
                        <?php echo form_error("problem", "<div class='error'>", "</div>"); ?>
                    </div>

                    <div class="col-xl-6 col-lg-6 col-12 form-group">
                        <label>Upload Document</label>
                        <input type="file" name="ticket_document[]" class="form-control" accept=".xlsx,.xls,image/*,.doc, .docx,.txt,.pdf" multiple="">
                    </div>
                    <div class="col-xl-9 col-lg-9 col-12 form-group">
                        <?php
                            if (isset($ticket_documents) && $ticket_documents != null) {
                            ?>
                                    <label>Ticket Document</label>
                                    <div class="img">
                                        <ul>
                                            <?php
                                                foreach ($ticket_documents as $key => $value) {
                                                    $id = $value['image_id'];           
                                                ?>
                                                    <li>
                                                        <a href="<?= base_url(TICKET_DOCUMENT.$value['ticket_document'])?>" download="<?= base_url(TICKET_DOCUMENT.$value['ticket_document'])?>">
                                                        <?php
                                                            $chek_file1 = array('docx','doc');
                                                            $chek_file2 = array('pdf');
                                                            $chek_file3 = array('png','jpeg','jpg');

                                                            if(in_array(pathinfo($value['ticket_document'], PATHINFO_EXTENSION), $chek_file1))
                                                            {
                                                                ?><img src="<?= base_url('assets/img/word_icon.png')?>" height="80" width="80"><br><?php
                                                            }
                                                            if(in_array(pathinfo($value['ticket_document'], PATHINFO_EXTENSION), $chek_file2)){
                                                                ?><img src="<?= base_url('assets/img/pdf_icon.jpg')?>" height="80" width="80"><br><?php
                                                            }

                                                            if(in_array(pathinfo($value['ticket_document'], PATHINFO_EXTENSION), $chek_file3)){
                                                                ?><img src="<?= base_url(TICKET_DOCUMENT.$value['ticket_document'])?>" height="80" width="80"><br><?php
                                                            }
                                                        ?>
                                                        </a>
                                                        <a href="<?= base_url('ticket/delete_image/'.$id)?>" title="Delete image" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash-o" aria-hidden="true" style="top: 0px; right: 0px; position: relative; color: #fff;"></i></a>
                                                    </li>
                                                <?php }
                                            ?>
                                        </ul>
                                    </div>
                            <?php }
                        ?>
                    </div>

                    <div class="col-12 form-group mg-t-8">
                        <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                        <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
<?php $this->view('authority/common/copyright'); ?>


<script>
    /*FORM VALIDATION*/
    $("#form").validate({
        rules: {          
            create_date: {required:true},          
            time: {required:true},          
            ticket_status_id: {required:true},
            ticket_location_id: {required:true},          
            priority_id: {required:true},           
            preferred_date: {required:true},           
            preferred_time: {required:true},           
            "ticket_problem_id[]": { 
                required: true, 
                minlength: 1 
            },
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            if(name == 'ticket_problem_id[]'){
                error.appendTo($("#ticket_problem_id"));
            } else {
                error.appendTo($("#" + name + "_validate"));
            }            
        },
        messages: {
            create_date: {required:"Please select date"},
            time: {required:"Please select time"},
            ticket_status_id: {required:"Please select ticket status"},
            ticket_location_id: {required:"Please select location"},
            priority_id: {required:"Please select priority"},
            preferred_date: {required:"Please select preferred date"},
            preferred_time: {required:"Please select preferred time"},
            "ticket_problem_id[]": "Please select at least one your problem",
        }
    });
</script>
<?php $this->view('authority/common/footer'); ?>
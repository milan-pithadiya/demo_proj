<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->view('authority/common/header');
echo'<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">';
$this->view('authority/common/sidebar');
?>
<div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <h3>Ticket</h3>
        <ul>
            <li>
                <a href="<?= base_url('authority') ?>">Home</a>
            </li>
            <li>Ticket Details</li>
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Student Details Area Start Here -->
    <div class="card height-auto">
        <div class="card-body">
            <div class="heading-layout1">
            </div>
            <div class="single-info-details">
                <div class="item-content">
                    <?php
                    if (isset($ticket_details) && $ticket_details != null) {
                        $ticket_id = $ticket_details[0]['id'];
                        $name = $ticket_details[0]['name'];
                        $location_name = $ticket_details[0]['location_name'];
                        $problem = $ticket_details[0]['problem'];
                        $ticket_problem_id = $ticket_details[0]['ticket_problem_id'];
                        $priority_name = $ticket_details[0]['priority_name'];
                        $preferred_time = $ticket_details[0]['preferred_time'];
                        $preferred_date = date('d-m-Y', strtotime($ticket_details[0]['preferred_date']));
                        $ticket_status = $ticket_details[0]['status_name'];
                        $ticket_no = $ticket_details[0]['ticket_no'];
                        $time = $ticket_details[0]['time'];
                        $create_date = date('d-m-Y', strtotime($ticket_details[0]['create_date']));

                        $expload_id = explode(',', $ticket_problem_id);
                        $ticket_problem = $this->Production_model->get_all_with_where_in('ticket_problem', 'id', 'desc', 'id', $expload_id);

                        $ticket_problem_name = array();
                        if (isset($ticket_problem) && $ticket_problem != null) {
                            foreach ($ticket_problem as $key => $value) {
                                $ticket_problem_name[] = $value['problem_name'];
                            }
                        }
                        $ticket_problem_name = implode(' , ', $ticket_problem_name);

                        /* Ticket allocated user details */
                        $ticket_allocated_user = array();
                        $ticket_allocate_dtls = $this->Production_model->get_all_with_where('ticket_allocate', 'id', 'desc', array('ticket_id' => $ticket_id));
                        if (isset($ticket_allocate_dtls) && $ticket_allocate_dtls != null) {
                            foreach ($ticket_allocate_dtls as $key => $value) {
                                $user_dtls = $this->Production_model->get_where_user('name,user_email', 'user_register', '', '', array('id' => $value['user_id']));
                                if (isset($user_dtls) && $user_dtls != null) {
                                    $ticket_allocated_user[] = $user_dtls[0]['name'];
                                }
                            }
                            $ticket_allocated_user = implode(' , ', $ticket_allocated_user);
                        }
                        /* End */
                    }
                    ?>

                    <div class="header-inline item-header">
                        <h3 class="text-dark-medium font-medium"><?= $name ?></h3>
                        <div class="header-elements">
                            <ul>
                                <!-- <li><a href="#"><i class="far fa-edit"></i></a></li>
                                <li><a href="#"><i class="fas fa-print"></i></a></li> -->
                                <!-- <li><a href="#"><i class="fas fa-download"></i></a></li> -->
                            </ul>
                        </div>
                    </div>
                    <div class="info-table table-responsive">
                        <table class="table text-nowrap">
                            <tbody>                                
                                <tr>
                                    <td>Ticket Create:</td>
                                    <td class="font-medium text-dark-medium"><?= $create_date ?></td>
                                </tr>
                                <?php
                                if (isset($ticket_allocated_user) && $ticket_allocated_user != null) {
                                    ?>
                                    <tr>
                                        <td>Ticket allocated user:</td>
                                        <td class="font-medium text-dark-medium"><?= $ticket_allocated_user ?></td>
                                    </tr>
                                <?php }
                                ?>
                                <tr>
                                    <td>Time:</td>
                                    <td class="font-medium text-dark-medium"><?= $time ?></td>
                                </tr>
                                <tr>
                                    <td>Ticket no:</td>
                                    <td class="font-medium text-dark-medium"><?= $ticket_no ?></td>
                                </tr>
                                <tr>
                                    <td>Ticket status:</td>
                                    <td class="font-medium text-dark-medium"><?= $ticket_status ?></td>
                                </tr>
                                <tr>
                                    <td>Location name:</td>
                                    <td class="font-medium text-dark-medium"><?= $location_name ?></td>
                                </tr>
                                <tr>
                                    <td>Preferred date:</td>
                                    <td class="font-medium text-dark-medium"><?= $preferred_date ?></td>
                                </tr>
                                <tr>
                                    <td>Preferred time:</td>
                                    <td class="font-medium text-dark-medium"><?= $preferred_time ?></td>
                                </tr>
                                <tr>
                                    <td>Priority:</td>
                                    <td class="font-medium text-dark-medium"><?= $priority_name ?></td>
                                </tr>
                                <tr>
                                    <td>Ticket problem:</td>
                                    <td class="font-medium text-dark-medium"><?= $ticket_problem_name ?></td>
                                </tr>
                                <tr>
                                    <td>Problem:</td>
                                    <td class="font-medium text-dark-medium" style="white-space: pre-wrap;"><?= $problem ?></td>
                                </tr>                                
                                <?php
                                if (isset($ticket_documents) && $ticket_documents != null) {
                                    echo '<tr>
                                    <td>Ticket Document:</td>';
                                    foreach ($ticket_documents as $key => $value) {
                                        $id = $value['image_id'];
                                        ?>
                                    <td class="font-medium text-dark-medium">
                                        <a href="<?= base_url(TICKET_DOCUMENT . $value['ticket_document']) ?>" download="<?= base_url(TICKET_DOCUMENT . $value['ticket_document']) ?>">
                                            <?php
                                            $chek_file1 = array('docx', 'doc');
                                            $chek_file2 = array('pdf');
                                            $chek_file3 = array('png', 'jpeg', 'jpg', 'webp');

                                            if (in_array(pathinfo($value['ticket_document'], PATHINFO_EXTENSION), $chek_file1)) {
                                                ?><img src="<?= base_url('assets/img/word_icon.png') ?>" height="80" width="80"><br><?php
                                            }
                                            if (in_array(pathinfo($value['ticket_document'], PATHINFO_EXTENSION), $chek_file2)) {
                                                ?><img src="<?= base_url('assets/img/pdf_icon.jpg') ?>" height="80" width="80"><br><?php
                                            }

                                            if (in_array(pathinfo($value['ticket_document'], PATHINFO_EXTENSION), $chek_file3)) {
                                                ?><img src="<?= base_url(TICKET_DOCUMENT . $value['ticket_document']) ?>" height="80" width="80"><?php
                                            }
                                            ?>
                                        </a>
                                    </td>
                                <?php
                                }
                                echo ' </tr>';
                            }
                            ?>                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->view('authority/common/copyright'); ?>

        <?php $this->view('authority/common/footer'); ?>									
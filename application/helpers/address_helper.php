<?php

function get_country() {
    $CI = & get_instance();
    $conditions = array("select" => "country_name,id", "where" => array("is_enable" => "1"), 'ORDER BY' => array('country_name' => 'ASC'));
    $info = $CI->common_model->select_data("country", $conditions);
    if ($info['row_count'] > 0) {
        return $info['data'];
    } else {
        return array();
    }
}

function get_state($id_country = '') {
    $CI = & get_instance();
    if ($id_country == '') {
        $conditions = array("select" => "state_name,id", "where" => array("is_enable" => "1"));
    } else {
        $conditions = array("select" => "state_name,id", "where" => array("is_enable" => "1", 'id_country' => $id_country));
    }
    $info = $CI->common_model->select_data("state", $conditions);
    if ($info['row_count'] > 0) {
        return $info['data'];
    } else {
        return array();
    }
}

function get_city($id_state = '') {
    $CI = & get_instance();
    if ($id_state == '') {
        $conditions = array("select" => "city_name,id", "where" => array("is_enable" => "1"));
    } else {
        $conditions = array("select" => "city_name,id", "where" => array("is_enable" => "1", 'id_state' => $id_state));
    }
    $info = $CI->common_model->select_data("city", $conditions);
    if ($info['row_count'] > 0) {
        return $info['data'];
    } else {
        return array();
    }
}

function getFormattedDate($date) {
    return $date;
    date_default_timezone_set('Asia/Kolkata');
    // return date('d M Y',strtotime($date));
}
function get_city_by_id($id) {
    $CI = & get_instance();
    $result = $CI->db->select("city_name")
        ->from('city')
        ->where('id',$id)
        ->get()->row_array();
    if (!empty($result)) {
        $result = $result['city_name'];
        return $result;
    } else {
        return array();
    }

}
function get_country_by_id($id) {
    $CI = & get_instance();
    $result = $CI->db->select("country_name")
        ->from('country')
        ->where('id',$id)
        ->get()->row_array();
    if (!empty($result)) {
        $result = $result['country_name'];
        return $result;
    } else {
        return array();
    }
}
function get_state_by_id($id) {
    $CI = & get_instance();  
    $result = $CI->db->select("state_name")
        ->from('state')
        ->where('id',$id)
        ->get()->row_array();
    if (!empty($result)) {
        $result = $result['state_name'];
        return $result;
    } else {
        return '';
    }

}

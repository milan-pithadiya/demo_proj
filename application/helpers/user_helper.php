<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*if (!function_exists('login')) {
    function login($email ='',$password ='',$is_api_call=''){
		$CI =& get_instance();
		$user_details = $CI->Production_model->get_all_with_where('user_register','','',array('email'=>$email));
		if ($user_details !=null) {
			if($user_details[0]['status'] == '1'){	 //0 deactive , 1 active
				$decrypt_pwd = $CI->encryption->decrypt($user_details[0]['password']);
				if ($decrypt_pwd == $password) {

					$sqlQuery = 'SELECT user.*, cr.country_name as country_id_info,st.state_name as state_id_info,ct.city_name as city_id_info FROM user_register as user, country cr ,state st,city ct WHERE user.id = "'.$user_details[0]['id'].'" AND cr.id=user.id_country AND st.id=user.id_state AND ct.id=user.id_city';

					$user_details = $CI->db->query($sqlQuery);
					$result = $user_details->result_array();			
					$result[0]['dob'] = date('d-m-Y',strtotime($result[0]['dob']));
					createResponse($is_api_call,"Success","Login Successfully",$result[0]);
				}else{
					createResponse($is_api_call,"Fail","Login Fail, password wrong",null);
				}
			}else{
				createResponse($is_api_call,"Fail","Login Fail, user is not active",null);
			}
		}else{
			createResponse($is_api_call,"Fail","Login Fail,no email address found",null);
		}
    }
}*/
if (!function_exists('login')) {
    function login($form_data){
		$CI =& get_instance();

		$is_api_call = isset($form_data['is_api_call']) ? $form_data['is_api_call'] : '';

    	$CI->form_validation->set_data($form_data);
    	$CI->form_validation->set_rules('email', '', 'trim|required|valid_email', array('required' => 'Please enter email'));
    	$CI->form_validation->set_rules('password', '', 'trim|required', array('required' => 'Please enter password'));
    	// $CI->form_validation->set_rules('g-recaptcha-response', '', 'trim|required', array('required' => 'Please select captcha'));

    	if ($CI->form_validation->run() === FALSE) {
	        foreach ($CI->form_validation->error_array() as $key => $value) {
	            // $response_array['message'] = array($key => $value);
	            return createResponse($is_api_call,"Fail",$value,null);
	            //$response_array['message'] = $value;
	            break;
	        }
	    } else {    	
	        
			$user_details = $CI->Production_model->get_all_with_where('user_register','','',array('email'=>$form_data['email']));
			if ($user_details !=null) {
				if($user_details[0]['status'] == '1'){	 //0 deactive , 1 active
					$decrypt_pwd = $CI->encryption->decrypt($user_details[0]['password']);
					if ($decrypt_pwd == $form_data['password']) {

						$sqlQuery = 'SELECT user.*, cr.country_name as country_id_info,st.state_name as state_id_info,ct.city_name as city_id_info FROM user_register as user, country cr ,state st,city ct WHERE user.id = "'.$user_details[0]['id'].'" AND cr.id=user.id_country AND st.id=user.id_state AND ct.id=user.id_city';
						
						$user_details = $CI->db->query($sqlQuery);
						$result = $user_details->result_array();			
						$result[0]['dob'] = date('d-m-Y',strtotime($result[0]['dob']));

						set_user_session('',$result[0]);
						return createResponse($is_api_call,"Success","Login Successfully",$result[0]);
					}else{
						return createResponse($is_api_call,"Fail","Login Fail, password wrong",null);
					}
				}else{
					return createResponse($is_api_call,"Fail","Login Fail, user is not active",null);
				}
			}else{
				return createResponse($is_api_call,"Fail","Login Fail,no email address found",null);
			}
		}
    }
}

/*if (!function_exists('registration')) {
    function registration($first_name ='',$last_name ='',$dob ='',	$email ='',	$password ='',	$address ='',	$city ='',	$state ='',	$country ='',$phone_number ='',	$zip_code ='',$is_api_call=''){
		$CI =& get_instance();
		$get_user = $CI->Production_model->get_all_with_where('user_register','','',array('email'=>$email));
		if (count($get_user) > 0) {
			createResponse($is_api_call,"Fail","Registration Fail, email already exist",null);		
		}
		$get_user = $CI->Production_model->get_all_with_where('user_register','','',array('phone_number'=>$phone_number));
		if (count($get_user) > 0) {
			createResponse($is_api_call,"Fail","Registration Fail, phone number already exist",null);
		}
		$create_date = date('Y-m-d h:i:s');
		$data = array(
			'first_name' => $first_name,
			'last_name' => $last_name,
			'dob' => date('Y-m-d h:i:s',strtotime($dob)),
			'password' => $CI->encryption->encrypt($password),
			'email' => $email,
			'address' => $address,
			'city' => $city,
			'state' => $state,
			'country' => $country,
			'zip_code' => $zip_code,
			'phone_number' => $phone_number,
			'status' => '0', //0 deactive , 1 active
			'create_date' => $create_date
		);
		$record=$CI->Production_model->insert_record('user_register',$data);
		$data['name']=$first_name." ".$last_name;
		$send_mail = $CI->Production_model->mail_send('Registration',$email,'','mail_form/thankyou_page/registration',$data,''); // Registration mail sends
		if($record !=''){
			createResponse($is_api_call,"Success","Registration Successfully.",array('id'=>(string) $record));
		}else{
			createResponse($is_api_call,"Fail","Error while Registration.",null); 
		}
	}
}*/
if (!function_exists('registration')) {
    function registration($form_data){
		$CI =& get_instance();
		$is_api_call = isset($form_data['is_api_call']) ? $form_data['is_api_call'] : '';

		$email = $form_data['email'];
		$first_name = $form_data['first_name'];
		$last_name = $form_data['last_name'];
		$password = $form_data['password'];
		$phone_number = $form_data['phone_number'];

    	$CI->form_validation->set_data($form_data);
    	$CI->form_validation->set_rules('first_name', '', 'trim|required', array('required' => 'Please enter first name'));
    	$CI->form_validation->set_rules('last_name', '', 'trim|required', array('required' => 'Please enter last name'));
    	$CI->form_validation->set_rules('email', '', 'trim|required|valid_email', array('required' => 'Please enter email'));
    	$CI->form_validation->set_rules('phone_number', '', 'trim|required', array('required' => 'Please enter phone number'));
    	$CI->form_validation->set_rules('password', '', 'trim|required', array('required' => 'Please enter password'));
    	$CI->form_validation->set_rules('g-recaptcha-response', '', 'trim|required', array('required' => 'Please select captcha'));

    	if ($CI->form_validation->run() === FALSE) {
	        foreach ($CI->form_validation->error_array() as $key => $value) {
	            // $response_array['message'] = array($key => $value);
	            return createResponse($is_api_call,"Fail",$value,null);
	            //$response_array['message'] = $value;
	            break;
	        }
	    } else {    

			$get_user = $CI->Production_model->get_all_with_where('user_register','','',array('email'=>$email));
			if (count($get_user) > 0) {
				return createResponse($is_api_call,"Fail","Registration Fail, email already exist",null);		
			}
			$get_user = $CI->Production_model->get_all_with_where('user_register','','',array('phone_number'=>$phone_number));
			if (count($get_user) > 0) {
				return createResponse($is_api_call,"Fail","Registration Fail, phone number already exist",null);
			}

			$form_data['dob']= date('Y-m-d',strtotime($form_data['dob']));
			$form_data['password']= $CI->encryption->encrypt($password);
			$form_data['create_date'] = date('Y-m-d h:i:s');
			$form_data['status'] = "0";//0 deactive , 1 active
			// $data = array(
			// 	'first_name' => $first_name,
			// 	'last_name' => $last_name,
			// 	'dob' => date('Y-m-d h:i:s',strtotime($dob)),
			// 	'password' => $CI->encryption->encrypt($password),
			// 	'email' => $email,
			// 	'address' => $address,
			// 	'city' => $city,
			// 	'state' => $state,
			// 	'country' => $country,
			// 	'zip_code' => $zip_code,
			// 	'phone_number' => $phone_number,
			// 	'status' => '0', //0 deactive , 1 active
			// 	'create_date' => $create_date
			// );
			if (isset($form_data['is_api_call'])) {
	            unset($form_data['is_api_call']);
	        }
	        if (isset($form_data['g-recaptcha-response'])) {
	            unset($form_data['g-recaptcha-response']);
	        }
	        if (isset($form_data['firstName'])) {
	            unset($form_data['firstName']);
	        }
	        if (isset($form_data['lastName'])) {
	            unset($form_data['lastName']);
	        }
	        if (isset($form_data['states'])) {
	            unset($form_data['states']);
	        }
	        if (isset($form_data['contactNumber'])) {
	            unset($form_data['contactNumber']);
	        }
	        if (isset($form_data['zipCode'])) {
	            unset($form_data['zipCode']);
	        }
	        if (isset($form_data['zipCode'])) {
	            unset($form_data['zipCode']);
	        }
	        if (isset($form_data['cpassword'])) {
	            unset($form_data['cpassword']);
	        }
			// echo "<pre>";print_r($form_data);exit;

			$record=$CI->Production_model->insert_record('user_register',$form_data);
			
			$data['name']=$first_name." ".$last_name;
			$data['user_id']=$record;
			$send_mail = $CI->Production_model->mail_send('Registration',$email,'','mail_form/thankyou_page/registration',$data,''); // Registration mail sends
			if($record !=''){
				$user_details = $CI->Production_model->get_all_with_where('user_register','','',array('id'=>$record));
				if(isset($user_details) && !empty($user_details)){
					// set_user_session('',$user_details[0]);
				}
				return createResponse($is_api_call,"Success","Registration Successfully.",array('id'=>(string) $record));
			}else{
				return createResponse($is_api_call,"Fail","Error while Registration.",null); 
			}
		}
	}
}

if (!function_exists('forgotPassword')) {
    function forgotPassword($email ='',$is_api_call=''){
		$CI =& get_instance();
		$user_details = $CI->Production_model->get_all_with_where('user_register','','',array('email'=>$email));
		if ($user_details != null) {
			$data = $user_details[0];
			$data['new_password'] = generate_password();
			
			if($user_details[0]['status'] == '1'){
				resetPassword($user_details[0]['id'],$data['new_password'],'');
				$send_mail = $CI->Production_model->mail_send('Forgot Password',$email,'','mail_form/forgot_password/forgot_email',$data,''); // forgot password user send mail
				return createResponse($is_api_call,"Success","Please check your mail to receive password releted instructions!",null);
			}else{
				return createResponse($is_api_call,"Fail","User is not active",null);
			}
		}else{
			return createResponse($is_api_call,"Fail","No email address found",null);
		}
    }
}

if (!function_exists('getProfile')) {
    function getProfile($id ='',$is_api_call=''){
		$CI =& get_instance();
		$sqlQuery = 'SELECT user.*, cr.country_name as country_id_info,st.state_name as state_id_info,ct.city_name as city_id_info FROM user_register as user, country cr ,state st,city ct WHERE user.id = "'.$id.'" AND cr.id=user.id_country AND st.id=user.id_state AND ct.id=user.id_city';
		$user_details = $CI->db->query($sqlQuery);
		$result = $user_details->result_array();
		// $result[0]['address'] = nl2br($result[0]['address']);
		// echo "<pre>";print_r($result);exit;
		if ($result !=null) {
			if($result[0]['status'] == '1'){	 //0 deactive , 1 active
				return createResponse($is_api_call,"Success","Found Successfully",$result[0]);
			}else{
				return createResponse($is_api_call,"Fail","User is not active",null);
			}
		}else{
			return createResponse($is_api_call,"Fail","No user found",null);
		}
    }
}

/*if (!function_exists('updateProfile')) {
    function updateProfile($id ='',$first_name ='',$last_name ='',$dob ='',	$email ='',	$address ='',	$city ='',	$state ='',	$country ='',$phone_number ='',	$zip_code ='',$is_api_call=''){		
		$CI =& get_instance();
		$data = array(
			'first_name' => $first_name,
			'last_name' => $last_name,
			'dob' => date('Y-m-d h:i:s',strtotime($dob)),
			'email' => $email,
			'address' => $address,
			'city' => $city,
			'state' => $state,
			'country' => $country,
			'zip_code' => $zip_code,
			'phone_number' => $phone_number
		);
		$record=$CI->Production_model->update_record('user_register',$data,array('id'=>$id));
	
		$sqlQuery = 'SELECT user.*, cr.country_name as country_id_info,st.state_name as state_id_info,ct.city_name as city_id_info FROM user_register as user, country cr ,state st,city ct WHERE user.id = "'.$id.'" AND cr.id=user.id_country AND st.id=user.id_state AND ct.id=user.id_city';
		$user_details = $CI->db->query($sqlQuery);
		$result = $user_details->result_array();			
		$result[0]['dob'] = date('d-m-Y',strtotime($result[0]['dob']));

		createResponse($is_api_call,"Success","Updated Successfully.",$result[0]);
	}
}*/


if (!function_exists('updateProfile')) {
    function updateProfile($form_data){
		$CI =& get_instance();
		$is_api_call = isset($form_data['is_api_call']) ? $form_data['is_api_call'] : '';

		$id = $form_data['id'];
		$email = $form_data['email'];
		$first_name = $form_data['first_name'];
		$last_name = $form_data['last_name'];
		$phone_number = $form_data['phone_number'];

    	$CI->form_validation->set_data($form_data);
    	$CI->form_validation->set_rules('first_name', '', 'trim|required', array('required' => 'Please enter first name'));
    	$CI->form_validation->set_rules('last_name', '', 'trim|required', array('required' => 'Please enter last name'));
    	$CI->form_validation->set_rules('email', '', 'trim|required|valid_email', array('required' => 'Please enter email'));
    	$CI->form_validation->set_rules('phone_number', '', 'trim|required', array('required' => 'Please enter phone number'));

    	if ($CI->form_validation->run() === FALSE) {
	        foreach ($CI->form_validation->error_array() as $key => $value) {
	            // $response_array['message'] = array($key => $value);
	            return createResponse($is_api_call,"Fail",$value,null);
	            //$response_array['message'] = $value;
	            break;
	        }
	    } else {    
			$form_data['dob']= date('Y-m-d',strtotime($form_data['dob']));

			// $data = array(
			// 	'first_name' => $first_name,
			// 	'last_name' => $last_name,
			// 	'dob' => date('Y-m-d h:i:s',strtotime($dob)),
			// 	'password' => $CI->encryption->encrypt($password),
			// 	'email' => $email,
			// 	'address' => $address,
			// 	'city' => $city,
			// 	'state' => $state,
			// 	'country' => $country,
			// 	'zip_code' => $zip_code,
			// 	'phone_number' => $phone_number,
			// 	'status' => '0', //0 deactive , 1 active
			// 	'create_date' => $create_date
			// );
			if (isset($form_data['is_api_call'])) {
	            unset($form_data['is_api_call']);
	        }
	        if (isset($form_data['id'])) {
	            unset($form_data['id']);
	        }
	        if (isset($form_data['firstName'])) {
	            unset($form_data['firstName']);
	        }
	        if (isset($form_data['lastName'])) {
	            unset($form_data['lastName']);
	        }
	        if (isset($form_data['country'])) {
	            unset($form_data['country']);
	        }
	        if (isset($form_data['states'])) {
	            unset($form_data['states']);
	        }
	        if (isset($form_data['city'])) {
	            unset($form_data['city']);
	        }
	        if (isset($form_data['contactNumber'])) {
	            unset($form_data['contactNumber']);
	        }
	        if (isset($form_data['zipCode'])) {
	            unset($form_data['zipCode']);
	        }
	        if (isset($form_data['zipCode'])) {
	            unset($form_data['zipCode']);
	        }
			// print_r($form_data);

	        $record=$CI->Production_model->update_record('user_register',$form_data,array('id'=>$id));
			
			if($record !=''){
				$user_details = $CI->Production_model->get_all_with_where('user_register','','',array('id'=>$id));
				if(isset($user_details) && !empty($user_details)){
					set_user_session('',$user_details[0]);
				}

				$sqlQuery = 'SELECT user.*, cr.country_name as country_id_info,st.state_name as state_id_info,ct.city_name as city_id_info FROM user_register as user, country cr ,state st,city ct WHERE user.id = "'.$id.'" AND cr.id=user.id_country AND st.id=user.id_state AND ct.id=user.id_city';
				$user_details = $CI->db->query($sqlQuery);
				$result = $user_details->result_array();			
				$result[0]['dob'] = date('d-m-Y',strtotime($result[0]['dob']));

				return createResponse($is_api_call,"Success","Profile updated Successfully.",array('id'=>(string) $id));
			}else{
				return createResponse($is_api_call,"Fail","Error while Profile update.",null); 
			}
		}
	}
}

if (!function_exists('changePassword')) {
    function changePassword($id ='',$old_password ='',$new_password ='',$is_api_call=''){		
		$CI =& get_instance();
		$data = array(
			'password' => $CI->encryption->encrypt($new_password),
		);

		$user_details = $CI->Production_model->get_all_with_where('user_register','','',array('id'=>$id));
		if ($user_details !=null) {
			if($user_details[0]['status'] == '1'){	 //0 deactive , 1 active
				$decrypt_pwd = $CI->encryption->decrypt($user_details[0]['password']);
				if ($decrypt_pwd == $old_password) {
					$record=$CI->Production_model->update_record('user_register',$data,array('id'=>$id));
					createResponse($is_api_call,"Success","Password Updated Successfully.",null);
				}else{
					createResponse($is_api_call,"Fail","Old password Not matched.",null);
				}
			}else{
				createResponse($is_api_call,"Fail","User is not active.",null);
			}
		}else{
			createResponse($is_api_call,"Fail","User is not found!.",null);
		}		
	}
}

if (!function_exists('resetPassword')) {
    function resetPassword($id ='',$new_password ='',$is_api_call=''){		
		$CI =& get_instance();
		$data = array(
			'password' => $CI->encryption->encrypt($new_password),
		);

		$user_details = $CI->Production_model->get_all_with_where('user_register','','',array('id'=>$id));
		if ($user_details !=null) {
			if($user_details[0]['status'] == '1'){	 //0 deactive , 1 active
				$record=$CI->Production_model->update_record('user_register',$data,array('id'=>$id));
				return createResponse($is_api_call,"Success","Password Reset Successfully.",null);
				
			}else{
				return createResponse($is_api_call,"Fail","User is not active.",null);
			}
		}else{
			return createResponse($is_api_call,"Fail","User is not found!.",null);
		}		
	}
}


if (!function_exists('feedback')) {
    function feedback($form_data){
		$CI =& get_instance();
	    $is_api_call = isset($form_data['is_api_call']) ? $form_data['is_api_call'] : '';

    	$CI->form_validation->set_data($form_data);
    	$CI->form_validation->set_rules('name', '', 'trim|required', array('required' => 'Please enter name'));
    	$CI->form_validation->set_rules('email', '', 'trim|required|valid_email', array('required' => 'Please enter email'));
    	$CI->form_validation->set_rules('subject', '', 'trim|required', array('required' => 'Please enter subject'));
    	$CI->form_validation->set_rules('message', '', 'trim|required', array('required' => 'Please enter message'));
    	$CI->form_validation->set_rules('phone_number', '', 'trim|required', array('required' => 'Please enter phone number'));
    	if ($CI->form_validation->run() === FALSE) {
	        foreach ($CI->form_validation->error_array() as $key => $value) {
	            // $response_array['message'] = array($key => $value);
	            return createResponse($is_api_call,"Fail",$value,null);
	            //$response_array['message'] = $value;
	            break;
	        }
	    }else{    	
	        if (isset($form_data['is_api_call'])) {
	            unset($form_data['is_api_call']);
	        }
	        if (isset($form_data['contactNumber'])) {
	            unset($form_data['contactNumber']);
	        }
			$form_data['status'] = '0';
			$form_data['create_date'] = date('Y-m-d h:i:s');

			$record=$CI->Production_model->insert_record('feedback',$form_data);
			if($record !=''){
				createResponse($is_api_call,"Success","Feedback Successfully.",array('id'=>(string) $record));
			}else{
				createResponse($is_api_call,"Fail","Error while Feedback.",null); 
			}
		}
	}
}
if (!function_exists('add_contact_us')) {
    function add_contact_us($form_data){
		$CI =& get_instance();
	    $is_api_call = isset($form_data['is_api_call']) ? $form_data['is_api_call'] : '';

    	$CI->form_validation->set_data($form_data);
    	$CI->form_validation->set_rules('name', '', 'trim|required', array('required' => 'Please enter name'));
    	$CI->form_validation->set_rules('email', '', 'trim|required|valid_email', array('required' => 'Please enter email'));
    	$CI->form_validation->set_rules('subject', '', 'trim|required', array('required' => 'Please enter subject'));
    	$CI->form_validation->set_rules('message', '', 'trim|required', array('required' => 'Please enter message'));
    	if ($CI->form_validation->run() === FALSE) {
	        foreach ($CI->form_validation->error_array() as $key => $value) {
	            // $response_array['message'] = array($key => $value);
	            return createResponse($is_api_call,"Fail",$value,null);
	            //$response_array['message'] = $value;
	            break;
	        }
	    }else{    	
	        if (isset($form_data['is_api_call'])) {
	            unset($form_data['is_api_call']);
	        }
			$form_data['status'] = '0';
			$form_data['create_date'] = date('Y-m-d h:i:s');

			$record=$CI->Production_model->insert_record('contact_us',$form_data);
			if($record !=''){
				return createResponse($is_api_call,"Success","Contact us inserted Successfully.",array('id'=>(string) $record));
			}else{
				return createResponse($is_api_call,"Fail","Error while Contact us.",null); 
			}
		}
	}
}
if (!function_exists('add_subscriber')) {
    function add_subscriber($form_data){
		$CI =& get_instance();
	    $is_api_call = isset($form_data['is_api_call']) ? $form_data['is_api_call'] : '';
    	$CI->form_validation->set_data($form_data);
    	$CI->form_validation->set_rules('email', '', 'trim|required|valid_email', array('required' => 'Please enter email'));
    	if ($CI->form_validation->run() === FALSE) {
	        foreach ($CI->form_validation->error_array() as $key => $value) {
	            // $response_array['message'] = array($key => $value);
	            return createResponse($is_api_call,"Fail",$value,null);
	            //$response_array['message'] = $value;
	            break;
	        }
	    }else{    	
	        if (isset($form_data['is_api_call'])) {
	            unset($form_data['is_api_call']);
	        }
			$form_data['status'] = '0';
			$form_data['create_date'] = date('Y-m-d h:i:s');
			$record=$CI->Production_model->insert_record('subscriber',$form_data);
			if($record !=''){
				createResponse($is_api_call,"Success","Subscribe added Successfully.",array('id'=>(string) $record));
			}else{
				createResponse($is_api_call,"Fail","Error while Subscribe added.",null); 
			}
		}
	}
}
if (!function_exists('set_user_session')) {
    function set_user_session($remember_me='',$user_data){
    	$CI =& get_instance();
		if ($remember_me == "yes") {
            $time = time() + 60 * 60 * 24;
            $cookie = array(
                'name' => 'USER',
                'value' => encrypt_cookie($email),
                'expire' => $time,
            );
            set_cookie($cookie);
            $cookie = array(
                'name' => 'PWD',
                'value' => encrypt_cookie($password),
                'expire' => $time,
            );
            set_cookie($cookie);
            $cookie = array(
                'name' => 'REMEMBER',
                'value' => encrypt_cookie("yes"),
                'expire' => $time,
            );
            set_cookie($cookie);
		}else{
		    delete_cookie("USER");
		    delete_cookie("PWD");
		    delete_cookie("REMEMBER");
		}
		// cookie end //
		$session = array(
			'login_id' => $user_data['id'],
			'username' => $user_data['first_name'],
			'useremail' => $user_data['email'],
			'useremobile' => $user_data['phone_number'],
		);		
		$CI->session->set_userdata($session);
	}
}

/*if (!function_exists('getPlans')) {
    function getPlans($is_api_call=''){
		$CI =& get_instance();
		$rec = array();	
		$linux = array();	
		$windows = array();	

		$plan_metadata= array();
		$plan_metadata1= array();
		$plan_detail = $CI->Production_model->get_all_with_where('announce','id','desc',array('status'=>"1"));
		// print_r($plan_detail);
		if($plan_detail !=null) {
			$linux_count=0;
			$windows_count=0;
			foreach ($plan_detail as $key => $value) {
				$data =  $CI->Production_model->get_all_with_where('announce_meta','id','desc',array('announce_id'=>$value['id']));
				
				if($value['plan_type'] == '1'){
					
					$linux[$linux_count]['id'] = $value['id'];			
					$linux[$linux_count]['name'] = $value['name'];
					$linux[$linux_count]['price'] = $value['price'];
					foreach ($data as $key1 => $value1) {
						if($value['id'] == $value1['announce_id']){
							$plan_metadata[$key1]['detail'] = $value1['data'];							
							$linux[$linux_count]['details'] = $plan_metadata;
						}
					}
					$linux_count ++;
				}
				
				if($value['plan_type'] == '2')
				{	
					$windows[$windows_count]['id'] = $value['id'];			
					$windows[$windows_count]['name'] = $value['name'];
					$windows[$windows_count]['price'] = $value['price'];
					foreach ($data as $key1 => $value1) {
						if($value['id'] == $value1['announce_id']){
							$plan_metadata1[$key1]['detail'] = $value1['data'];
							$windows[$windows_count]['details'] = $plan_metadata1;
						}
					}
					$windows_count ++;
				}
			}
			$rec["windows"]=$windows;
			$rec["linux"]=$linux;
			// echo "<pre>";print_r($rec);

			
			createResponse($is_api_call,"Success","",$rec);
		}else{
			createResponse($is_api_call,"Fail","No Plan found!",null);
		}
	}
}*/
/*if (!function_exists('generate_ticket')) {
    function generate_ticket($user_id='', $name ='', $email ='',$subject ='', $message='', $department='',$related_service='' ,$priority='',$new_file_name, $is_api_call=''){
		$CI =& get_instance();
		// $user_details = $CI->Production_model->get_all_with_where('user_register','','',array('id'=>$id));
		$create_date = date('Y-m-d h:i:s');
		$data = array(
			'user_id'			=> $user_id,
			'name'				=> $name,
			'email'				=> $email,
			'subject'			=> $subject,
			'department'		=> $department,
			'related_service'	=> $related_service,
			'message'			=> $message,
			'user_file'			=> $new_file_name,
			'priority'			=> $priority,
			'status'			=> '0', //0 deactive , 1 active
			'create_date'		=> $create_date
		);
		$record=$CI->Production_model->insert_record('tickets',$data);
		if($record !=''){
			createResponse($is_api_call,"Success","Ticket generated Successfully.",array('id'=>(string) $record));
		}else{
			createResponse($is_api_call,"Fail","Error while Ticket generating.",null); 
		}
	}
}*/

/*if (!function_exists('getHostingDetails')) {
    function getHostingDetails($is_api_call=''){
		$CI =& get_instance();
		$rec = array();		
		$plan_metadata= array();
		$plan_detail = $CI->Production_model->get_all_with_where('hosting_plan','id','desc',array('status'=>"1"));
		// print_r($plan_detail);
		if ($plan_detail !=null) {
			foreach ($plan_detail as $key => $value) {
				$rec[$key]['id'] = $value['id'];			
				$rec[$key]['name'] = $value['name'];
				$rec[$key]['image'] = $value['image'];
			}

			createResponse($is_api_call,"Success","",$rec);
		}else{
			createResponse($is_api_call,"Fail","No Plan found!",null);
		}
	}
}*/

/*if (!function_exists('getCustomerSupport')) {
    function getCustomerSupport($is_api_call=''){
		$CI =& get_instance();
		$rec = array();		
		$country_details = $CI->Production_model->get_all_record('','country','');
		if ($country_details !=null) {
			foreach ($country_details as $key => $value) {
				$rec[$key]['id'] = $value['id'];
				$rec[$key]['name'] = $value['country_name'];					
			}


			createResponse($is_api_call,"Success","",$rec);
		}else{
			createResponse($is_api_call,"Fail","No country found!",null);
		}
    }
}*/

/*if (!function_exists('getDepartments')) {
    function getDepartments($is_api_call=''){
		$CI =& get_instance();
		$rec = array();		
		$plan_metadata= array();
		$plan_detail = $CI->Production_model->get_all_with_where('departments','id','desc',array('status'=>"1"));
		// print_r($plan_detail);
		if ($plan_detail !=null) {
			foreach ($plan_detail as $key => $value) {
				$rec[$key]['id'] = $value['id'];			
				$rec[$key]['name'] = $value['name'];
				$rec[$key]['description'] = $value['description'];
			}

			createResponse($is_api_call,"Success","",$rec);
		}else{
			createResponse($is_api_call,"Fail","No Plan found!",null);
		}
	}
}*/

/*if (!function_exists('getMyPlans')) {
    function getMyPlans($user_id,$is_api_call=''){
		$CI =& get_instance();
		$rec = array();		
		$plan_metadata= array();
		// $plan_detail = $CI->Production_model->get_all_with_where('user_plans','id','desc',array('status'=>"1",'user_id'=>$user_id));

			$where['user_plans.status'] = "1";
			$where['user_plans.user_id'] = $user_id;

            $join[0]['table_name'] = 'user_register';
            $join[0]['column_name'] = 'users.id = user_plans.user_id';
            $join[0]['type'] = 'left';

            $user_plan_detail = $CI->Production_model->jointable_descending(array('user_plans.*','users.id as user_id','concat(users.first_name ," ",users.last_name) as name','users.email'),'user_plans','',$join,'user_plans.id','desc',$where,array(),'','','');
        
            // echo"<pre>"; echo $CI->db->last_query(); print_r($user_plan_detail); exit;

		
		
		if ($user_plan_detail !=null) {

			
				`id`, `type`, `user_id`, `domain_id`, `domain_name`, `provider_id`, `username`, `password`, `client_name`, `client_notification_email`, `client_phone`, `admin_notify_email`, `book_date`, `expiry_date`, `name_server`, `price`, `notification_off`, `status`, `created`, `updated`
			
			foreach ($user_plan_detail as $key => $value) {
				$rec[$key]['id'] = $value['id'];			
				$rec[$key]['name'] = $value['name'];
				$rec[$key]['email'] = $value['email'];
				$rec[$key]['domain_id'] = $value['domain_id'];
				$rec[$key]['username'] = $value['username'];
				$rec[$key]['password'] = $value['password'];
				$rec[$key]['provider_id'] = $value['provider_id'];
				$rec[$key]['name_server'] = $value['name_server'];
				$rec[$key]['price'] = $value['price'];
				$rec[$key]['book_date'] = $value['book_date'];
				$rec[$key]['expiry_date'] = $value['expiry_date'];
				$rec[$key]['status'] = $value['status'];
				
			}
			createResponse($is_api_call,"Success","",$rec);
		}else{
			createResponse($is_api_call,"Fail","No Plan found!",null);
		}
	}
}*/

/*if (!function_exists('getCountry')) {
    function getCountry($is_api_call=''){
		$CI =& get_instance();
		$rec = array();		
		$country_details = $CI->Production_model->get_all_record('','country','');
		if ($country_details !=null) {
			foreach ($country_details as $key => $value) {
				$rec[$key]['id'] = $value['id'];
				$rec[$key]['name'] = $value['country_name'];					
			}
			createResponse($is_api_call,"Success","",$rec);
		}else{
			createResponse($is_api_call,"Fail","No country found!",null);
		}
    }
}
if (!function_exists('getStates')) {
    function getStates($country_id ='',$is_api_call=''){
		$CI =& get_instance();
		$rec = array();		
		$state_detail = $CI->Production_model->get_all_with_where('state','id','asc',array('id_country'=>$country_id));
		if ($state_detail !=null) {
			foreach ($state_detail as $key => $value) {
				$rec[$key]['id'] = $value['id'];
				$rec[$key]['name'] = $value['state_name'];					
			}
			createResponse($is_api_call,"Success","",$rec);
		}else{
			createResponse($is_api_call,"Fail","No states found!",null);
		}
	}
}
if (!function_exists('getCities')) {
    function getCities($state_id ='',$is_api_call=''){
		$CI =& get_instance();
		$rec = array();		
		$city_detail = $CI->Production_model->get_all_with_where('city','id','asc',array('id_state'=>$state_id));
		if ($city_detail !=null) {
			foreach ($city_detail as $key => $value) {
				$rec[$key]['id'] = $value['id'];			
				$rec[$key]['name'] = $value['city_name'];					
			}
			createResponse($is_api_call,"Success","",$rec);
		}else{
			createResponse($is_api_call,"Fail","No city found!",null);
		}
	}
}*/
if (!function_exists('add_search_domain')) {
    function add_search_domain($form_data){
		$CI =& get_instance();
		// echo "<pre>";print_r($form_data);exit;
		$is_api_call = isset($form_data['is_api_call']) ? $form_data['is_api_call'] : '';

		$email = $form_data['email'];
    	$CI->form_validation->set_data($form_data);
    	$CI->form_validation->set_rules('name', '', 'trim|required', array('required' => 'Please enter name'));
    	$CI->form_validation->set_rules('email', '', 'trim|required|valid_email', array('required' => 'Please enter email','valid_email' => 'Please enter proper email'));
    	$CI->form_validation->set_rules('mobile_no', '', 'trim|required', array('required' => 'Please enter mobile number'));

    	if ($CI->form_validation->run() === FALSE) {
	        foreach ($CI->form_validation->error_array() as $key => $value) {
	            // $response_array['message'] = array($key => $value);
	            return createResponse($is_api_call,"Fail",$value,null);
	            //$response_array['message'] = $value;
	            break;
	        }
	    } else {    
			$form_data['create_date'] = date('Y-m-d h:i:s');
			$form_data['status'] = "0";
			if (isset($form_data['is_api_call'])) {
	            unset($form_data['is_api_call']);
	        }
			// print_r($form_data);
			$record=$CI->Production_model->insert_record('searched_domain',$form_data);
			// $send_mail = $CI->Production_model->mail_send('Searched Domain',$email,'','mail_form/thankyou_page/searched_domain',$form_data,''); // Registration mail sends
			if($record !=''){
				return createResponse($is_api_call,"Success","Searched domain added Successfully.",array('id'=>(string) $record));
			}else{
				return createResponse($is_api_call,"Fail","Error while Searching domain.",null); 
			}
		}
	}
}

if (!function_exists('getAccessToken')) {
    function getAccessToken($email ='',$is_api_call=''){
		$CI =& get_instance();
		
		$user_details = $CI->Production_model->get_all_with_where('user','','',array('email_address'=>$email));
		if ($user_details != null) {
			$data = $user_details[0];
			$data['access_token'] = generate_password();
			
			if($user_details[0]['status'] == '1'){
				$record=$CI->Production_model->update_record('user',$data,array('email_address'=>$email));
				if($record){
					$send_mail = $CI->Production_model->mail_send('Access Token',$email,'','mail_form/access_token',$data,''); // Access Token to user send mail
					return createResponse($is_api_call,"Success","Please check your mail to receive login releted instructions!",null);
				}else{
					return createResponse($is_api_call,"Fail","Access token not sent",null);	
				}
			}else{
				return createResponse($is_api_call,"Fail","User is not active",null);
			}
		}else{
			return createResponse($is_api_call,"Fail","No email address found",null);
		}
    }
}


?>

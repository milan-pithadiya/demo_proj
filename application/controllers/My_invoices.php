<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class My_invoices extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();			
		}
		function index()
		{
			$data['invoice_data'] = $this->Production_model->get_all_with_where('invoice','id','desc',array('user_id'=>$this->session->login_id));
			// echo "<pre>".$this->db->last_query();print_r($data);exit;
			$this->load->view('my_invoices',$data);
		}
		function view_invoice($id){
			$where['invoice.id'] = $id;
			$join1[0]['table_name'] = 'invoice_item';
            $join1[0]['column_name'] = 'invoice_item.invoice_id = invoice.id';
            $join1[0]['type'] = 'left';
            
            $data['invoice_details'] = $this->Production_model->jointable_descending(array('invoice.*','invoice_item.description','invoice_item.price','invoice_item.quantity','invoice_item.total'), 'invoice', '', $join1, 'invoice.id', 'desc', $where);
			$data['invoice_details'] = $this->Production_model->get_all_with_where('user','','',array());
			$this->load->view('invoice',$data);	
		}
	}
?>
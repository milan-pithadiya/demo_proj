<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class How_it_work extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();			
		}

		function index()
		{
			$data['work_details'] = $this->Production_model->get_all_with_where('how_it_work','id','desc',array('status'=>'1'));
			$this->load->view('how_it_work',$data);	
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>
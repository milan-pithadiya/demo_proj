<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->load->library('form_validation');
        $this->load->library('common_functions');
    }

    public function view($page_number = "") {
        $settings = array(
            "url" => site_url() . "authority/user/view/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "id,full_name,email_address,mobile_number_1,status","NOT IN" => array("id" => 1));
        $data = $this->common_model->get_pagination("user", $conditions, $settings);
        if (isset($this->session->user_msg)) {
            $data = array_merge($data, array("success" => $this->session->user_msg));
            $this->session->unset_userdata('user_msg');
        }
        unset($settings, $conditions);
        $this->load->view('authority/user/view', $data);
    }

    public function add_edit($id = "") {
        $this->load->helper("form");
        if ($id != "") {
            $data = array("form_title" => "Edit Profile");
        }else{
            $data = array("form_title" => "Add Subadmin");
        }
        if ($id != "") {
            $conditions = array("where" => array("id" => $id));
            $user_info = $this->common_model->select_data("user", $conditions);
            if ($user_info['row_count'] > 0) {
                $data = array_merge($data, $user_info['data'][0]);
            } else {
                // redirect("authority/user/view");
            }
        } else {
            // redirect("authority/user/view");
        }

        /* Form Validation */
        
        $this->validate($id,$data);
        if ($this->form_validation->run() === FALSE) {
            $data = array_merge($data, $_POST);
        } else {
            $error = false;
            $records = $_POST;
            // if (isset($_FILES['profile_photo']) && $_FILES['profile_photo']['name'] != "") {
            //     $config['upload_path'] = PROFILE_PHOTO;
            //     $config['allowed_types'] = 'gif|jpg|png|jpeg|"';                
            //     $this->load->library('upload', $config);
            //     if (!$this->upload->do_upload('profile_photo')) {
            //         $error_msg = array('error' => $this->upload->display_errors());
            //         $error = true;
            //     } else {
            //         /* delete old photo */
            //         if (isset($data['profile_photo']) && !is_null($data['profile_photo'])) {
            //             $path = $config['upload_path'] . $data['profile_photo'];
            //             if (is_file($path)) {
            //                 @unlink($path);
            //             }
            //         }
            //         $upload_data = $this->upload->data(); 
            //         //Returns array of containing all of the data related to the file you uploaded.
            //         $file_name = $upload_data['file_name'];
            //         chmod($config['upload_path'] . $file_name, 0777);
            //         $records["profile_photo"] = $file_name;
            //     }
            // } else {
            //     $records["profile_photo"] = $data['profile_photo'];
            // }
            /*footer logo*/
            /*if (isset($_FILES['footer_logo']) && $_FILES['footer_logo']['name'] != "") {
                $config['upload_path'] = PROFILE_PHOTO;
                $config['allowed_types'] = 'gif|jpg|png|jpeg|"';                
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('footer_logo')) {
                    $error_msg = array('error' => $this->upload->display_errors());
                    $error = true;
                } else {
                    
                    if (!is_null($data['footer_logo'])) {
                        $path = $config['upload_path'] . $data['footer_logo'];
                        if (is_file($path)) {
                            @unlink($path);
                        }
                    }
                    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                    $file_name = $upload_data['file_name'];
                    chmod($config['upload_path'] . $file_name, 0777);
                    $records["footer_logo"] = $file_name;
                }
            } else {
                $records["footer_logo"] = $data['footer_logo'];
            }
            echo"<pre>"; print_r($records); exit;*/
            if (!$error) {
                if($id ==''){
                    if ($_FILES['profile_photo']['name'] !='') {

                        $records['profile_photo'] = $this->Production_model->image_upload(PROFILE_PHOTO,'profile_photo');
                    }
                    
                    // echo "<pre>";print_r($_FILES);exit;
                    $this->common_model->insert_data("user", $records);
                    $msg ="Record inserted successfully";
                    
                }else{
                    if ($_FILES['profile_photo']['name'] !='') {
                        $records['profile_photo'] = $this->Production_model->image_upload(PROFILE_PHOTO,'profile_photo','user',$data['id']);
                    }
                    $conditions = array(
                        "where" => array("id" => $data['id']),
                    );
                    // echo "<pre>";print_r($records);exit;
                    $this->common_model->update_data("user", $records, $conditions);
                    $msg ="Record updated successfully";
                    // if ($data['id'] == $this->session->user_info['id']) {
                    //     $this->common_functions->updatesession($data['id']);
                    //     $msg ="Record updated successfully";
                    // }
                }
                $data = array_merge($data, $_POST);
                $data = array_merge($data, array("success" => $msg));
                $_POST = array();
                $data = array_merge($data, $_POST);
                // redirect('authority/user/view/','refresh');
            }
        }
        // echo"<pre>"; print_r($data); exit;
        $this->load->view('authority/user/add-edit', $data);
    }

    public function delete($id = "") {
        if ($id != "") {
            $conditions = array("select" => "id,profile_photo", "where" => array("id" => intval($id)));
            $user = $this->common_model->select_data("user", $conditions);
            if ($user['row_count'] > 0) {
                $photo = $user['data'][0]['profile_photo'];
                if (!is_null($photo)) {
                    $path = "./uploads/profile_photo/" . $photo;
                    if (is_file($path)) {
                        @unlink($path);
                    }
                }
            }
            $conditions = array(
                "where" => array("id" => $id),
            );
            $this->common_model->delete_data("user", $conditions);
            $this->session->user_msg = "Record deleted successfully";
            redirect("authority/user/view");
        } else {
            redirect("authority/user/view");
        }
    }

    public function details($id = "") {
        $data = array();
        if ($id != "") {
            $conditions = array("where" => array("id" => $id));
            $user_info = $this->common_model->select_data("user", $conditions);
            if ($user_info['row_count'] > 0) {
                $data = array_merge($data, $user_info['data'][0]);
            } else {
                redirect("authority/user/view");
            }
        } else {
            redirect("authority/user/view");
        }
        $this->load->view('authority/user/details', $data);
    }
    public function validate($id,$data){
        $this->form_validation->set_rules('full_name', 'full name', 'required', array('required' => 'Please enter user name'));
        if(!empty($id)){
            $this->form_validation->set_rules('email_address', 'email addres', 'required|valid_email|is_unique_with_except_record[user.email_address.id.' . $data['id'] . ']', array('required' => 'Please enter email address', "valid_email" => "Please enter valid email address", "is_unique_with_except_record" => "This email is already available"));
        }else{
            $this->form_validation->set_rules('email_address', 'email addres', 'required|valid_email|is_unique[user.email_address.id]', array('required' => 'Please enter email address', "valid_email" => "Please enter valid email address", "is_unique_with_except_record" => "This email is already available"));
        }
        
        $this->form_validation->set_rules('mobile_number_1', 'mobile number', 'required|min_length[10]', array('required' => 'Please enter mobile number'));
        $this->form_validation->set_rules('mobile_number_2', 'mobile number', 'min_length[10]');
    }
    function reset_password($id){
        if($id){
            $new_password = $this->input->post('password');
            $conditions = array(
                "where" => array("id" => $id),
            );
            $records = array(
                'password' => $records['password'] = sha1($this->session->user_info['salt'].$new_password),
            );
            $user_details = $this->Production_model->get_all_with_where('user','','',array('id'=>$id));
            if ($user_details !=null) {
                if($user_details[0]['status'] == '1'){   //0 deactive , 1 active
                    $reset = $this->common_model->update_data("user", $records, $conditions);
                    if($reset != ''){
                        $this->session->set_flashdata('success', 'Password Reset Successfully...!');
                    }else{
                        $this->session->set_flashdata('error', 'Reset not applied...!');
                    }
                }else{
                    $this->session->set_flashdata('error', 'User is not active...!');
                }
            }
            redirect(base_url('authority/user/view'));
            // echo"<pre>"; print_r($data); exit;
        }
    }

}

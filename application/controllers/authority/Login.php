<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->load->helper('cookie');
    }

    public function index() {
        if ($this->session->loggedin) {
//            if ($this->session->loggedin_user_role == "User") {
//                redirect("user/dashboard");
//            }
            if ($this->session->loggedin_user_role == "Admin") {
                redirect("authority/dashboard");
            }
        }

        $data = array();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email_address', 'Email address', 'trim|required|valid_email', array("required" => "Please enter email address", 'valid_email' => 'Please enter a valid email address'));
        // $this->form_validation->set_rules('access_token', 'access token', 'trim|required', array("required" => "Please enter access token"));
        $this->form_validation->set_rules('password', 'password', 'trim|required', array("required" => "Please enter password"));
       
        if ($this->form_validation->run()) {
            $this->load->library('encryption');
            $email_address = $this->input->post('email_address');
            $password = $this->input->post('password');
            $access_token = $this->input->post('access_token');

            $conditions = array("where" => array("email_address" => $email_address));
            $info = $this->common_model->select_data("user", $conditions);
            if ($info['row_count'] > 0) {
                $info = $info['data'][0];

                if ($info['is_enable'] == "1" && $info['status'] == 1) {

                    if (sha1($info['salt'] . $password) != $info['password']) {
                        $data['message'] = "Please enter correct username and password";
                        $data['email_address'] = $email_address;
                        $data['password'] = $password;
                        $data['access_token_msg'] = "Please re-enter access token";
                    }elseif ($info['access_token'] != $access_token) {
                        $data['access_token_msg'] = "Access token is incorrect";
                        $data['email_address'] = $email_address;
                        $data['password'] = $password;
                    } else {
                        if ($this->input->post("remember_me") == "yes") {
                            $time = time() + 60 * 60 * 24;
                            $cookie = array(
                                'name' => 'NC_USER',
                                'value' => encrypt_cookie($email_address),
                                'expire' => $time,
                            );
                            set_cookie($cookie);
                            $cookie = array(
                                'name' => 'NC_PWD',
                                'value' => encrypt_cookie($password),
                                'expire' => $time,
                            );
                            set_cookie($cookie);
                            $cookie = array(
                                'name' => 'NC_REMEMBER',
                                'value' => encrypt_cookie("yes"),
                                'expire' => $time,
                            );
                            set_cookie($cookie);
                        } else {
                            delete_cookie("NC_USER");
                            delete_cookie("NC_PWD");
                            delete_cookie("NC_REMEMBER");
                        }
                        $this->session->loggedin = true;
                        $this->session->loggedin_user_role = $info['role'];
                        $this->session->user_info = $info;
                        if ($this->session->loggedin_user_role == "Admin") {
                            redirect('authority/dashboard');
                        } else {
                            redirect('user/dashboard'); 
                        }
                    }
                } else {
                    $data['message'] = "Your account is disabled";
                }
            } else {
                $data['message'] = "Please enter correct username and password";
            }
        }
        /* FOR CHECKING REMEMBER ME DETAILS */
        if (get_cookie("NC_REMEMBER")) {
            if (decrypt_cookie(get_cookie("NC_REMEMBER")) == "yes") {
                $data['email_address'] = decrypt_cookie(get_cookie("NC_USER"));
                $data['password'] = decrypt_cookie(get_cookie("NC_PWD"));
                $data['remember_me'] = "yes";
            }
        }
        $this->load->view('authority/login', $data);
    }

    public function logout() {
        if ($this->session->loggedin) {
            $this->session->sess_destroy();
        }
        redirect("authority/login");
    }

    public function forgot_password() {
        $data = array();
        $this->load->helper('form');
        $this->load->library('form_validation');

        if ($this->input->method() == "post") {
            $this->form_validation->set_rules('email_address', 'Email address', 'trim|required|valid_email|callback_is_email_avl', array("required" => "Please enter email address", 'valid_email' => 'Please enter a valid email address'));
            if ($this->form_validation->run()) {

                $email_address = $this->input->post("email_address");
                $conditions = array("where" => array("email_address" => $email_address));
                $info = $this->common_model->select_data("user", $conditions);
                $info = $info['data'][0];
                $this->load->helper('string');
                $data['password'] = $password = random_string('alnum', 8);
                $records = array(
                    'password' => sha1($info['salt'] . $password),
                );
                $conditions = array(
                    "where" => array("email_address" => $email_address),
                );
                $this->common_model->update_data("user", $records, $conditions);
                $data = array_merge($data, array("success" => "Your new password has been generated. Please check email for your new password."));

                $send_mail = $this->Production_model->mail_send('Forgotpassword',$info['email_address'],'','mail_form/forgot_password/forgot_email',$data,'');

            }
        }

        $this->load->view('authority/forgot-password', $data);
    }

    public function is_email_avl($email_address) {
        $conditions = array("where" => array("email_address" => $email_address));
        $info = $this->common_model->select_data("user", $conditions);
        if ($info['row_count'] > 0) {
            $info = $info['data'][0];
            if ($info['is_enable'] != "1") {
                $this->form_validation->set_message(
                        'is_email_avl', 'Your account is not activate.'
                );
                return false;
            } else {
                return true;
            }
        } else {
            $this->form_validation->set_message(
                    'is_email_avl', 'This email address is not available in our record.'
            );
            return false;
        }
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Add_posted extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    // private $search_options = array(
    //     'post_title' => 'title',
    //     'name' => 'user name',
    // );

    // private $search_status = array(
    //     '1' => 'approve',
    //     '0' => 'reject',
    //     '2' => 'active',
    //     '3' => 'deactive',
    // );

    public function index($page_number='') {
        /* For clearing a search */
        if ($this->input->get('clear_filter') == 1) {
            $this->session->post_filter = array();
            redirect(base_url() . 'authority/report/add-posted');
        }
        if ($this->input->method(TRUE) == 'POST') {
            $post_filter = $this->input->post();
            // echo"<pre>"; print_r($post_filter); 
        } else if (isset($this->session->post_filter) && !empty($this->session->post_filter)) {
            $post_filter = $this->session->post_filter;
            // echo"<pre>"; print_r($post_filter); exit;
        } else {
            $post_filter = array();
        }

        $search_query = '';       

        // /* For post approve,reject,active,deactive status */
        // if (isset($post_filter['search_options']) && $post_filter['search_options'] != '' &&
        //         isset($post_filter['search_value']) && $post_filter['search_value'] != '') {

            if (isset($post_filter['category_id']) && $post_filter['category_id'] !='') {
                $search_query .= ' AND pm.category_id =' . $post_filter['category_id'] . ' ';
                // $search_query .= ' AND pm.category_id = "1" ';
            } 
            if (isset($post_filter['sub_category_id']) && $post_filter['sub_category_id'] !='') {
                $search_query .= ' AND pm.sub_category_id =' . $post_filter['sub_category_id'] . ' ';
            }
            if (isset($post_filter['city_id']) && $post_filter['city_id'] !='') {
                $search_query .= ' AND ur.id_city =' . $post_filter['city_id'] . ' ';
            }
            // echo"<pre>"; print_r($post_filter); exit;
        // }

        /* For post start date filter */
        if (isset($post_filter['start_date']) && $post_filter['start_date'] != '' && isset($post_filter['end_date']) && $post_filter['end_date'] != '') {
            $start_date = date('Y-m-d', strtotime($post_filter['start_date']));
            $end_date = date('Y-m-d', strtotime($post_filter['end_date']));
            $search_query .= ' AND DATE(pm.create_date) BETWEEN "' . $start_date . '" AND "' . $end_date . '" ';
        }
        
        // $sql = 'SELECT pm.*,ur.name,ur.user_id,ur.id_city FROM post_management pm, user_register ur WHERE ur.user_id = pm.user_id' . $search_query . ' ORDER BY pm.post_id desc';

        $sql = 'SELECT pm.*,ur.name,ur.user_email,ur.mobile_no,ur.user_id,ur.id_city,pkg.name as package_name,pkg.day,pkg.price as package_price,pkg.currency,pkg.discription,cntry.country_name,s.state_name,c.city_name FROM post_management pm, user_register ur , package_management pkg , country cntry, state s, city c  WHERE ur.user_id = pm.user_id AND pkg.id = pm.package_id AND cntry.id = ur.id_country AND s.id = ur.id_state AND c.id = ur.id_city' . $search_query . ' ORDER BY pm.post_id desc';
        $post_filter['filter_sql'] = $sql;
        
        $data = $this->common_model->get_data_with_sql($sql);
        // echo $this->db->last_query(); echo"<pre>"; print_r($data); exit; 

        if ($data['row_count'] > 0) {
            $settings = array(
                "total_record" => $data['row_count'],
                'url' => site_url() . 'authority/report/add-posted/index/',
                "per_page" => RECORDS_PER_PAGE,
            );
            /* GET PAGINATION */
            $info = $this->common_model->only_pagination($settings);
            $sql .= ' LIMIT ' . $info['start'] . ', ' . $info['limit'] . ' ';
            $data = $this->common_model->get_data_with_sql($sql);
            $data['pagination'] = $info['pagination'];
        }

        // $data = array_merge($data, array('search_options' => $this->search_options));
        // $data = array_merge($data, array('search_status' => $this->search_status));
        $this->session->post_filter = $post_filter;

        isset($info['start']) ? $data['no'] = $info['start']+1 : '';
        unset($settings, $conditions);
        /*End*/

        // echo"<pre>"; print_r($data); exit;       
        $this->load->view('authority/report/post_report', $data);
    }

    function edit($id)
    {
        $data['category_details'] = $this->Production_model->get_all_with_where('category','','',array('status'=>'1'));
        $data['post_details'] = $this->Production_model->get_all_with_where('post_management','','',array('post_id'=>$id));
        $data['similar_img_details'] = $this->Production_model->get_all_with_where('post_similer_image','','',array('post_id'=>$id));
        // echo"<pre>"; print_r($data['similar_img_details']); exit;
        $this->load->view('authority/report/post-add-edit',$data);
    }

    function delete($id)
    {
        $get_image = $this->Production_model->get_all_with_where('product_manage','','',array('product_id'=>$id));

        if ($get_image !=null && $get_image[0]['product_image'] !=null && !empty($get_image[0]['product_image']) && $get_image[0]['product_image'] !='')
        {
            @unlink(PRODUCT_IMAGE.$get_image[0]['product_image']);
            @unlink(PRODUCT_IMAGE.'thumbnail/medium/'.$get_image[0]['product_image']);
            @unlink(PRODUCT_IMAGE.'thumbnail/thumb/'.$get_image[0]['product_image']);
        }

        $get_smlr_image = $this->Production_model->get_all_with_where('product_similar_image','','',array('product_id'=>$id));
        if ($get_smlr_image !=null) {
            foreach ($get_smlr_image as $key => $img_row) {
                @unlink(PRDCT_SIMILAR_IMG.$img_row['product_similar_image']);
                @unlink(PRDCT_SIMILAR_IMG.'thumbnail/'.$img_row['product_similar_image']);
                $tmp_record = $this->Production_model->delete_record('product_similar_image',array('product_id'=>$img_row['product_id']));
            }
        }
        $record = $this->Production_model->delete_record('product_manage',array('product_id'=>$id));

        if ($record == 1) {
            $this->session->set_flashdata('success', 'Product Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Product Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');

        foreach ($chkbox_id as $key => $value) {
           /*Old image delete*/
           $get_image = $this->Production_model->get_all_with_where('post_similer_image','','',array('post_id'=>$value));
            
            if ($get_image !=null && $get_image[0]['similer_image'] !=null && !empty($get_image[0]['similer_image']))
            {
               @unlink(POST_NEED_IMG.$get_image[0]['similer_image']);
               @unlink(POST_NEED_IMG.'thumbnail/'.$get_image[0]['similer_image']);
               // @unlink(CAT_MEDIUM_IMAGE.$get_image[0]['similer_image']);
            }
            $record = $this->Production_model->delete_record('post_management',array('post_id'=>$value));
            $record = $this->Production_model->delete_record('post_similer_image',array('post_id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Post Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Post Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    } 

    function excel_genrate_report()
    {   
        $tmp = [];
        // ==================== pagination start ======================== //
        /* For clearing a search */
        if ($this->input->get('clear_filter') == 1) {
            $this->session->post_filter = array();
            redirect(base_url() . 'authority/report/add-posted');
        }
        if ($this->input->method(TRUE) == 'POST') {
            $post_filter = $this->input->post();
            // echo"<pre>"; print_r($post_filter); 
        } else if (isset($this->session->post_filter) && !empty($this->session->post_filter)) {
            $post_filter = $this->session->post_filter;
            // echo"<pre>"; print_r($post_filter); exit;
        } else {
            $post_filter = array();
        }

        $search_query = '';       

        // /* For post approve,reject,active,deactive status */
        // if (isset($post_filter['search_options']) && $post_filter['search_options'] != '' &&
        //         isset($post_filter['search_value']) && $post_filter['search_value'] != '') {

            if (isset($post_filter['category_id']) && $post_filter['category_id'] !='') {
                $search_query .= ' AND pm.category_id =' . $post_filter['category_id'] . ' ';
                // $search_query .= ' AND pm.category_id = "1" ';
            } 
            if (isset($post_filter['sub_category_id']) && $post_filter['sub_category_id'] !='') {
                $search_query .= ' AND pm.sub_category_id =' . $post_filter['sub_category_id'] . ' ';
            }
            if (isset($post_filter['city_id']) && $post_filter['city_id'] !='') {
                $search_query .= ' AND ur.id_city =' . $post_filter['city_id'] . ' ';
            }
            // echo"<pre>"; print_r($post_filter); exit;
        // }

        /* For post start date filter */
        if (isset($post_filter['start_date']) && $post_filter['start_date'] != '' && isset($post_filter['end_date']) && $post_filter['end_date'] != '') {
            $start_date = date('Y-m-d', strtotime($post_filter['start_date']));
            $end_date = date('Y-m-d', strtotime($post_filter['end_date']));
            $search_query .= ' AND DATE(pm.create_date) BETWEEN "' . $start_date . '" AND "' . $end_date . '" ';
        }
        
        // $sql = 'SELECT pm.*,ur.name,ur.user_id,ur.id_city FROM post_management pm, user_register ur WHERE ur.user_id = pm.user_id' . $search_query . ' ORDER BY pm.post_id desc';

        $sql = 'SELECT pm.*,ur.name,ur.user_email,ur.mobile_no,ur.user_id,ur.id_city,pkg.name as package_name,pkg.day,pkg.price as package_price,pkg.currency,pkg.discription,cntry.country_name,s.state_name,c.city_name FROM post_management pm, user_register ur , package_management pkg , country cntry, state s, city c  WHERE ur.user_id = pm.user_id AND pkg.id = pm.package_id AND cntry.id = ur.id_country AND s.id = ur.id_state AND c.id = ur.id_city' . $search_query . ' ORDER BY pm.post_id desc';
        $post_filter['filter_sql'] = $sql;
        
        $data = $this->common_model->get_data_with_sql($sql);
        // echo $this->db->last_query(); echo"<pre>"; print_r($data); exit; 

        if ($data['row_count'] > 0) {
            $settings = array(
                "total_record" => $data['row_count'],
                'url' => site_url() . 'authority/report/add-posted/index/',
                "per_page" => RECORDS_PER_PAGE,
            );
            /* GET PAGINATION */
            $info = $this->common_model->only_pagination($settings);
            $sql .= ' LIMIT ' . $info['start'] . ', ' . $info['limit'] . ' ';
            $data = $this->common_model->get_data_with_sql($sql);
            $data['pagination'] = $info['pagination'];
        }

        // $data = array_merge($data, array('search_options' => $this->search_options));
        // $data = array_merge($data, array('search_status' => $this->search_status));
        $this->session->post_filter = $post_filter;

        isset($info['start']) ? $data['no'] = $info['start']+1 : '';
        unset($settings, $conditions);
        /*End*/
        // echo"<pre>"; print_r($data); exit;

        $tmp = array_merge($tmp,$data);
        // echo"<pre>"; print_r($tmp['data']); exit;

        // excel export data strart //
        $excel_data = [];
        foreach ($tmp['data'] as $key => $value) {
            /*Get category and sub-category*/
            // echo"<pre>"; print_r($value);
            $tmp1 = $this->Production_model->get_all_with_where('category','','',array('category_id'=>$value['category_id']));

            $tmp2 = $this->Production_model->get_all_with_where('sub_category','','',array('sub_category_id'=>$value['sub_category_id']));

            $visitor_count = $this->Production_model->get_where_user('post_id','post_visitor','','',array('post_id'=>$value['post_id']));

            if ($value['post_approve_reject'] == '1') {
                $post_status = "Approve";
            }
            if ($value['post_approve_reject'] == '0') {
                $post_status = "Reject";
            }
            $tmp = array(
                'Name' => $value['name'],
                'User Email' => $value['user_email'],
                'Location' => $value['country_name'].' , '.$value['state_name'].' , '.$value['city_name'],
                'Mobile No' => $value['mobile_no'],
                'Post Category' => $tmp1 !=null ? $tmp1[0]['category_name'] : '',
                'Post Sub Category' => $tmp2 !=null ? $tmp2[0]['sub_category_name'] : '',
                'Status of ads' => $post_status,
                'Tittle of the ad' => $value['i_want'],
                'View on ads' => count($visitor_count),
                'Create Date' => date('d-m-Y',strtotime($value['create_date'])),
            );          
            array_push($excel_data,$tmp);
        }
        // echo"<pre>"; print_r($excel_data); exit;
        function filterData(&$str)
        {
            $str = preg_replace("/\t/", "\\t", $str);
            $str = preg_replace("/\r?\n/", "\\n", $str);
            if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
        }
        // file name for download
        $fileName = "Post-report-" . date('d-m-Y') . ".xls";
        // headers for download
        header("Content-Disposition: attachment; filename=\"$fileName\"");
        header("Content-Type: application/vnd.ms-excel");
        $flag = false;
        foreach($excel_data as $row) {
            if(!$flag) {
                // display column names as first row
                echo implode("\t", array_keys($row)) . "\n";
                $flag = true;
            }
            // filter data
            array_walk($row, 'filterData');
            echo implode("\t", array_values($row)) . "\n";
        }
        exit;
        // excel export data end //
    }

    // ajax call category wise all subcategory list start // 

    function get_sub_cat_name() {
        $cat_id = $this->input->post('category_id');
        $sub_cat_id = $this->session->post_filter['sub_category_id'];

        $get_sub_cat = $this->Production_model->get_all_with_where('sub_category', 'sub_category_id', 'asc', array('category_id' => $cat_id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($get_sub_cat); exit;
        ?><option value=''>Select</option><?php
        if ($get_sub_cat != null) {
            foreach ($get_sub_cat as $key => $sub_cat_row) {
                ?><option value="<?= $sub_cat_row['sub_category_id'] ?>" <?= ($sub_cat_row['sub_category_id'] == $sub_cat_id) ? 'selected' : ''; ?>>
                    <?php
                    echo $sub_cat_row['sub_category_name'];
                    ?>
                </option><?php
            }
        }
    }

    // ajax call category wise all subcategory list end //  
}
?>
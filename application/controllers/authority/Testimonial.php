<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonial extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function index($page_number='') {
        $settings = array(
            "url" => site_url() . "authority/testimonial/index/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data = $this->common_model->get_pagination("testimonial", $conditions, $settings);
        // echo"<pre>"; print_r($data); exit;
        if (isset($this->session->category_msg) && $this->session->category_msg != '') {
            $data = array_merge($data, array("success" => $this->session->category_msg));
            $this->session->category_msg = '';
        }
        unset($settings, $conditions);
        // echo"<pre>"; print_r($data); exit;
        
        $this->load->view('authority/testimonial/view', $data);
    }

    function add()
    {
        $data['testimonial_details'] = array();       
        $this->load->view('authority/testimonial/add-edit',$data);
    }

    // ==================================================================== //
    // ================= excel formate download && insert ================= //
    // ==================================================================== //

    // public function download_excel_testimonial(){

    //     // At the beggining...
    //     ob_start();    
    //     $content="";
    //     $normalout=true;

    //     // ... do some stuff ...

    //     // i guess if some condition is true...
    //     $file_name = 'category.xls';
    //     $content=ob_get_clean();
    //     $normalout=false;
    //     header( "Content-Type: application/vnd.ms-excel" );
    //     header( "Content-disposition: attachment; filename=".$file_name);
    //     echo 'Category Name English'. "\t" .'Category Name Arabic' . "\n";
    //     // echo 'First Name' . "\t" . 'Last Name' . "\t" . 'Phone' . "\n";
    //     // echo 'John' . "\t" . 'Doe' . "\t" . '555-5555' . "\n";


    //     // Here you could die() or continue...
    //     ob_start();
    //     // ... rest of execution ...

    //     $content.=ob_get_clean();
    //     if($normalout)
    //     {
    //         echo($content);
    //     } else {
    //         // Excel provided no output.
    //     }
    // }

    // public function save_excel_testimonial()
    // {  
    //     $this->load->library('excel');
    //     if ($_FILES['xls_file']['error'] == 0) {

    //         $ext = pathinfo($_FILES['xls_file']['name'], PATHINFO_EXTENSION);
           
    //         $imagePath = CATEGORY_EXCEL;
    //         $img_name = 'category_excel-'.rand(111,999)."-".str_replace(' ', '_',$_FILES['xls_file']['name']);

    //         $destFile = $imagePath . $img_name; 
    //         $filename = $_FILES["xls_file"]["tmp_name"];       
    //         move_uploaded_file($filename, $destFile);
            
    //         if(is_array($img_name)){
    //             $this->session->set_flashdata('phperror','File Not Uploaded.!!!'.$img_name['error']);
    //             redirect($_SERVER['HTTP_REFERER']);
    //         }

    //         $path = CATEGORY_EXCEL;
    //         $data['FileName'] = $img_name;
    //         $data['FieldsList'] =array("name_english","name_arabic");

    //         $excel_data = $this->excel->import_excel($path,$data);
    //         // echo '<pre>'; print_r($excel_data); exit;

    //         $resultSet = Array(); 

    //         //======== check duplicate value get in excel sheet data start ========//

    //         $tmp_resultSet = Array(); 
    //         $duplicates = Array(); 
    //         foreach ($excel_data as $key => $dt) {

    //             if (in_array($dt, $tmp_resultSet)) {
    //                 $duplicates[] = $dt;
    //             }
    //             $tmp_resultSet[] = $dt;
    //         }
    //         $tmp = $duplicates;
    //         $duplicate_cat_name = array();
    //         if ($tmp !=null) {
    //             foreach ($tmp as $key => $duplicate_row){
    //                 $tmp = $duplicate_row['name_english'];
    //                 array_push($duplicate_cat_name,$tmp);
    //             }
    //             if (!empty($duplicate_cat_name)){
    //                 $error_cat_name = implode(', ', array_unique($duplicate_cat_name));

    //                 // echo"<pre>"; print_r(array_unique($duplicate_cat_name)); exit;
    //                 $this->session->set_flashdata('error',"$error_cat_name Duplicate category name...!");
    //                 redirect(base_url('authority/testimonial/add'));
    //             }
    //         }
               
    //         //========= check duplicate value get in excel sheet data end =========//

    //         foreach ($excel_data as $key => $dt) {
    //             echo"<pre>"; print_r($dt); 

    //             $get_cat_name = $this->Production_model->get_all_with_where('testimonial','','',array('name_english'=>$dt['name_english']));

    //             if(!empty($get_cat_name)) 
    //             {
    //                 $resultSet[] = $dt['name_english'];
    //                 // echo"<pre>"; print_r($resultSet);
    //             }
    //         }

    //         if (!empty($resultSet)) {
    //             // echo"<pre>"; print_r($resultSet); exit;

    //             $error = implode(', ', $resultSet);

    //             $this->session->set_flashdata('error',"$error Category name is allredy exist...!");
    //             redirect(base_url('authority/testimonial/add'));
    //         }
    //         // echo"<pre>"; print_r($excel_data); exit;
    //         else
    //         {
    //             foreach ($excel_data as $key => $dt) {
    //                 $ins = array(
    //                     'name_english'=>$dt['name_english'],
    //                     'create_date' => date('Y-m-d H:i:s')
    //                 ); //insert Array

    //                 $this->Production_model->insert_record('testimonial',$ins);//Insert Table Code
    //             }
    //         }

    //         $this->session->set_flashdata('success','Excel Imported.!!!');
    //         redirect(base_url('authority/testimonial/view'));

    //         // echo '<pre>'; print_r($excel_data); exit;
    //     } else {
    //         $this->session->set_flashdata('error','File Not Uploaded.!!!');
    //         redirect($_SERVER['HTTP_REFERER']);
    //     }
    // }

    // ==================================================================== //
    // =========================== End of this ============================ //
    // ==================================================================== //

    function insert_testimonial()
    {
        $data = $this->input->post();
        $data['create_date'] = date('Y-m-d H:i:s');
       
        /*testimonial image*/
        if($_FILES['testimonial_image']['name']==""){ $img_name='';}
        else{
            $imagePath = TESTIMONIAL_IMAGE;
            if(!is_dir($imagePath)){
                mkdir($imagePath);
                mkdir(TESTIMONIAL_ROOT_UPLOAD_PATH);
                // mkdir(CAT_MEDIUM_IMAGE);
                mkdir(TESTIMONIAL_THUMB_IMAGE);
                @chmod($imagePath,0777);
            }

            $img_name = 'TESTIMONIAL-IMG'.rand(111,999)."-".str_replace(' ', '_',$_FILES['testimonial_image']['name']);
            $destFile = $imagePath . $img_name; 

            $filename = $_FILES["testimonial_image"]["tmp_name"];       
            move_uploaded_file($filename,  $destFile);

            // create Thumbnail -- IMAGE_SIZES start //
            $image_sizes = array(
                // 'mobile'=>array(75,75),
                'thumb' => array(50, 50), // Category IMAGE DISPLAY.
                // 'medium' => array(370, 275), // Category IMAGE DISPLAY.
                // 'large' => array(900, 500)
            );

            $this->load->library('image_lib');
            foreach ($image_sizes as $key=>$resize) {
                $config = array(
                    'source_image' => TESTIMONIAL_IMAGE.$img_name,
                    'new_image' => TESTIMONIAL_ROOT_UPLOAD_PATH .'/'.$key,
                    'maintain_ratio' => FALSE,
                    'width' => $resize[0],
                    'height' => $resize[1],
                    'quality' =>70,
                );
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();
                // echo"<pre>"; print_r($config); 
            }            
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

            // create Thumbnail -- IMAGE_SIZES end //
            $data['testimonial_image'] = $img_name;
        }
        // echo"<pre>"; print_r($data); exit;
        $duplicate_records = $this->Production_model->get_all_with_where('testimonial','','',array('name'=>$data['name']));
        if (count($duplicate_records) > 0) {
            $this->session->set_flashdata('error', 'Name allredy exist....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else{
            $inserted_id = $this->Production_model->insert_record('testimonial',$data);

            if ($inserted_id !='') {
                $this->session->set_flashdata('success', 'Testimonial Add Successfully....!');
                redirect(base_url('authority/testimonial'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Testimonial Not Added....!');
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }

    function edit($id)
    {
        $data['testimonial_details'] = $this->Production_model->get_all_with_where('testimonial','','',array('id'=>$id));
        $this->load->view('authority/testimonial/add-edit',$data);
    }

    function update_testimonial()
    {
        $data = $this->input->post();
        $data['modified_date'] = date('Y-m-d H:i:s');
        $testimonial_id = $this->input->post('id');
        // echo"<pre>"; print_r($data); exit;

        $this->form_validation->set_rules('name', 'category name', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER['HTTP_REFERER']); 
        }
        else
        {
            $get_image = $this->Production_model->get_all_with_where('testimonial','','',array('id'=>$testimonial_id));
            // echo"<pre>"; print_r($get_image); exit;

            /*testimonial image*/
            if($_FILES['testimonial_image']['name'] !=''){            
                $imagePath = TESTIMONIAL_IMAGE;
                if(!is_dir($imagePath)){
                    mkdir($imagePath);
                    mkdir(TESTIMONIAL_ROOT_UPLOAD_PATH);
                    // mkdir(CAT_MEDIUM_IMAGE);
                    mkdir(TESTIMONIAL_THUMB_IMAGE);
                    @chmod($imagePath,0777);
                }
                if ($get_image !=null && $get_image[0]['testimonial_image'] !=null && !empty($get_image[0]['testimonial_image']))
                {
                   @unlink(TESTIMONIAL_IMAGE.$get_image[0]['testimonial_image']);
                   // @unlink(CAT_MEDIUM_IMAGE.$get_image[0]['testimonial_image']);
                   @unlink(TESTIMONIAL_THUMB_IMAGE.$get_image[0]['testimonial_image']);
                }

                $img_name = 'TESTIMONIAL-IMG'.rand(111,999)."-".str_replace(' ', '_',$_FILES['testimonial_image']['name']);
                $destFile = $imagePath . $img_name; 

                $filename = $_FILES["testimonial_image"]["tmp_name"];       
                move_uploaded_file($filename,  $destFile);

                // create Thumbnail -- IMAGE_SIZES start //
                $image_sizes = array(
                    // 'mobile'=>array(75,75),
                    'thumb' => array(50, 50), // Category IMAGE DISPLAY.
                    // 'medium' => array(370, 275), // Category IMAGE DISPLAY.
                    // 'large' => array(900, 500)
                );

                $this->load->library('image_lib');
                foreach ($image_sizes as $key=>$resize) {
                    $config = array(
                        'source_image' => TESTIMONIAL_IMAGE.$img_name,
                        'new_image' => TESTIMONIAL_ROOT_UPLOAD_PATH .'/'.$key,
                        'maintain_ratio' => FALSE,
                        'width' => $resize[0],
                        'height' => $resize[1],
                        'quality' =>70,
                    );
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();
                    $this->image_lib->clear();
                    // echo"<pre>"; print_r($config); 
                }            
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                // create Thumbnail -- IMAGE_SIZES end //
                $data['testimonial_image'] = $img_name;
            }
            // echo"<pre>"; print_r($data); exit;

            $record = $this->Production_model->update_record('testimonial',$data,array('id'=>$testimonial_id));
            if ($record == 1) {
                $this->session->set_flashdata('success', 'Testimonial Update Successfully....');
                redirect(base_url('authority/testimonial'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Testimonial Not Updated....');
                redirect($_SERVER['HTTP_REFERER']);
            }   
        }
    }

    function delete_testimonial($id)
    {
        /*Old image delete*/
        $get_image = $this->Production_model->get_all_with_where('testimonial','','',array('id'=>$id));
        // echo"<pre>"; print_r($get_image); exit;
        
        if ($get_image !=null && $get_image[0]['testimonial_image'] !=null && !empty($get_image[0]['testimonial_image']))
        {
           @unlink(TESTIMONIAL_IMAGE.$get_image[0]['testimonial_image']);
           @unlink(TESTIMONIAL_THUMB_IMAGE.$get_image[0]['testimonial_image']);
           // @unlink(CAT_MEDIUM_IMAGE.$get_image[0]['testimonial_image']);
        }
        $record = $this->Production_model->delete_record('testimonial',array('id'=>$id));
      
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Testimonial Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Testimonial Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');

        foreach ($chkbox_id as $key => $value) {
           /*Old image delete*/
           $get_image = $this->Production_model->get_all_with_where('testimonial','','',array('id'=>$value));
            
            if ($get_image !=null && $get_image[0]['testimonial_image'] !=null && !empty($get_image[0]['testimonial_image']))
            {
               @unlink(TESTIMONIAL_IMAGE.$get_image[0]['testimonial_image']);
               @unlink(TESTIMONIAL_THUMB_IMAGE.$get_image[0]['testimonial_image']);
               // @unlink(CAT_MEDIUM_IMAGE.$get_image[0]['testimonial_image']);
            }
            $record = $this->Production_model->delete_record('testimonial',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Testimonial Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Testimonial Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>
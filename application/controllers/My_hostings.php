<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class My_hostings extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();			
		}

		function index()
		{
			
	        $data['user_hostings_details'] = $this->Production_model->get_all_with_where('user_hosting','','',array('user_id'=>$this->session->login_id));
	        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
	        $data['user_details'] = $this->common_model->select_data("user_register", $conditions);
	        unset($settings, $conditions);        

	        /*$where['user_hosting.user_id'] = $this->session->login_id;
			
			$join[0]['table_name'] = 'user_register';
			$join[0]['column_name'] = 'user_register.id = user_hosting.user_id';
			$join[0]['type'] = 'left';

			$join[1]['table_name'] = 'domain';
			$join[1]['column_name'] = 'domain.id = user_hosting.domain_type_id';
			$join[1]['type'] = 'left';

			$data['user_hostings_details'] = $this->Production_model->jointable_descending(array('user_register.first_name','user_register.last_name', 'user_hosting.id','domain.title'), 'user_hosting', '', $join, 'user_hosting.id', 'desc', $where);*/
			// echo "<pre>".$this->db->las_query();print_r($data);exit;
			$this->load->view('my_hostings',$data);
		}
	}
?>
<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class My_domains extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();			
		}

		function index()
		{
			// $data['user_domains_details'] = $this->Production_model->get_all_with_where('user_domain','','',array('user_id'=>$this->session->login_id));

	  //       $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
	  //       $data['user_details'] = $this->common_model->select_data("user_register", $conditions);
	  //       unset($settings, $conditions); 
	        $where['user_domain.user_id'] = $this->session->login_id;

			$join[0]['table_name'] = 'user_register';
			$join[0]['column_name'] = 'user_register.id = user_domain.user_id';
			$join[0]['type'] = 'left';

	        $join[1]['table_name'] = 'domain';
			$join[1]['column_name'] = 'domain.id = user_domain.domain_type_id';
			$join[1]['type'] = 'left';

			$data['user_domains_details'] = $this->Production_model->jointable_descending(array('user_domain.*','user_register.first_name','user_register.last_name', 'user_domain.id','domain.title'), 'user_domain', '', $join, 'user_domain.id', 'desc', $where);

			// echo"<pre>";echo $this->db->last_query(); print_r($data); exit;
			$this->load->view('my_domains',$data);
		}
		function register_new_domain(){
			$data['admin_details'] = $this->Production_model->get_all_with_where('user','','',array());
			$this->load->view('register_new_domain',$data);	
		}
	}
?>
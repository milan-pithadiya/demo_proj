<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Faq extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();			
		}

		function index()
		{
			$data['faq_details'] = $this->Production_model->get_all_with_where('faq','id','desc',array('status'=>'1'));
			$this->load->view('faq',$data);	
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Email_model extends CI_model {

    public function __construct() {
        
    }

    public function get_email_header() {
        return "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
        <html xmlns='http://www.w3.org/1999/xhtml'>
            <head>
                <meta http-equiv='' content='' />
                <meta name='' content='' />
                <title>" . SITE_TITLE . "</title>
                <style type='text/css'>
                    body { margin:0; padding:0; color:#333333; font-family:Arial, Helvetica, sans-serif; font-size:14px; width:100% !important; }
                </style>
            </head>
            <body>
            <table width='95%' cellpadding='0' cellspacing='0' style='color:rgb(34,34,34);border-width:1px;border-style:solid;border-collapse:collapse; border-color:rgb(221,221,221);' align='center'>
                <tr>
                    <td>
                        <table width='100%' align='center' cellpadding='10' cellspacing='0' style='background:rgb(250, 250, 250)'>
                            <tr>
                                <td align='center' valign='middle'>
                                    <a href='" . site_url() . "' target='_blank'>
                                        <img src='" . site_url() . "assets/images/logo.png' width='255' height='65' border='0' />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style='padding:10px;'>";
    }

    public function get_email_footer() {
        return "</td>
                        </tr>
                        <tr>
                            <td valign='top' style='border-top-width:1px;border-top-color:rgb(221,221,221);border-top-style:solid;padding:10px;background:rgb(250,250,250)'><table width='100%' border='0' cellspacing='0' cellpadding='0' style=''>
                        <tr>
                            <td height='10' align='left' valign='middle'></td>
                        </tr>
                        <tr>
                            <td height='25' align='left' valign='middle'>Thanks</td>
                        </tr>
                        <tr>
                            <td height='10' align='left' valign='middle'></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
    </html>";
    }

}

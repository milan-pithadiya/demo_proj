<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);
define('IMAGES', 'assets/image/'); // user-profile.
define('PROFILE_PICTURE', 'assets/uploads/profile_picture/'); // user-profile.
define('PROFILE_PHOTO', 'assets/uploads/profile_photo/'); // user-profile.

define('TICKET_FILE', 'assets/uploads/ticket_files/');
define('TICKET_REPLY_FILE', 'assets/uploads/ticket_reply_files/');
define('TICKET_DOCUMENT', 'assets/uploads/ticket_docs/'); // 
define('UPLOADS_PATH', 'assets/uploads/');
define('HOME_SLIDER', 'assets/uploads/home_slider/');
// define('PRODUCT_IMAGE', 'assets/uploads/product_image/');
// define('PRDCT_SIMILAR_IMG', 'assets/uploads/product_similar_image/');
define('POST_NEED_IMG', 'assets/uploads/post_need_image/');
define('TESTIMONIAL_IMAGE', 'assets/uploads/testimonial_image/');

define('CAT_IMAGE', 'assets/uploads/category_image/');
define('HOSTING_IMAGE', 'assets/uploads/hosting_plan/');
define('POST_IMAGE', 'assets/uploads/post_image/');
define('ABOUT_IMAGE', 'assets/uploads/about_us/');
define('PAGE_IMAGE', 'assets/uploads/page_image/');
define('BLOG_IMAGE', 'assets/uploads/blog_image/');
define('NEWS_LETTER_IMAGE', 'assets/uploads/news_letter_image/');
define('MAIL_SEND_IMAGE', 'assets/uploads/mail_send_image/');
define('POST_INVOICE', 'order_details_page/');

// thumbnil create image start //

// $root = "http://" . $_SERVER['HTTP_HOST'];
// define('LARGE_IMAGE', 'assets/uploads/category_image/thumbnail/large/');
define('CAT_MEDIUM_IMAGE', 'assets/uploads/category_image/thumbnail/medium/');
define('CAT_THUMB_IMAGE', 'assets/uploads/category_image/thumbnail/thumb/');
// define('MOBILE_IMAGE', 'assets/uploads/category_image/thumbnail/mobile/');
define('CAT_ROOT_UPLOAD_PATH','assets/uploads/category_image/thumbnail');

// define('LARGE_IMAGE', 'assets/uploads/home_slider/thumbnail/large/');
define('MEDIUM_IMAGE', 'assets/uploads/home_slider/thumbnail/medium/');
define('THUMB_IMAGE', 'assets/uploads/home_slider/thumbnail/thumb/');
// define('MOBILE_IMAGE', 'assets/uploads/home_slider/thumbnail/mobile/');
define('ROOT_UPLOAD_PATH','assets/uploads/home_slider/thumbnail');

define('TESTIMONIAL_THUMB_IMAGE', 'assets/uploads/testimonial_image/thumbnail/thumb/');
define('TESTIMONIAL_ROOT_UPLOAD_PATH','assets/uploads/testimonial_image/thumbnail');

// thumbnil create image end //

// define('FCMKey', 'AAAA0xuQR6Y:APA91bFqYkD_lhzkmKgI6cVBLVrvAAzf-k6mdy0Xu7Fu16sxYU7W3cvOSV6ymnjyqAQ1Rcy9Jqe_b3pxa4ECGeOzbHEYZVZ9jrIXA3L5r8lXJ_sJB_hg-VsWdMHZ0-fd7T6ISHH0pEB8'); 

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/**/
define('MAX_FILE_SIZE_IMAGE',10000000); // 
define('RECORDS_PER_PAGE',10); // admin
define('RECORDS_PER_PAGE_FRONT',10); // front

// define('CASHFREE_MODE','TEST');
// define('CASHFREE_APP_ID','4172f8687c25053149477a622714');
// define('CASHFREE_SECRET_KEY','146edcf2d6a593557a5a8db411f0d8fab667e897');